<?php
  use_helper('I18N');
?>  

<?php if($sf_user->isAuthenticated()):?>
<div id='sf_admin_theme_footer_1'>
<?php  image_tag('cultura.jpg'); ?>
</div>

<div id='sf_admin_theme_footer_2' >
<?php  image_tag('logo2.jpg'); ?>
</div>
<?php else: ?>
<div id='sf_admin_theme_footer_1'>
<?php  image_tag('logos2.jpg'); ?>
</div>

<div id='sf_admin_theme_footer_2' >
<?php  image_tag('LOGO3.jpg'); ?>
</div>
<?php endif?>


<div id='sf_admin_theme_footer'>
  <?php echo __('Copyright &copy; %current_year% %site_name%. All rights reserved', array('%current_year%' => date('Y'), '%site_name%' => sfAdminDash::getProperty('site'))); ?>
</div>
