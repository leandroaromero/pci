<?php

/**
 * Manifestacion filter form base class.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseManifestacionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'proyecto_id'                                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Proyecto'), 'add_empty' => true)),
      'localizacion_id'                              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localizacion'), 'add_empty' => true)),
      'denominacion'                                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'observaciones'                                => new sfWidgetFormFilterInput(),
      'area_expresion_id'                            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AreaExpresion'), 'add_empty' => true)),
      'nombre_en_otra_area'                          => new sfWidgetFormFilterInput(),
      'caracteristicas_resumen'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'caracteristicas_descripcion'                  => new sfWidgetFormFilterInput(),
      'caracteristicas_permanencia_transformaciones' => new sfWidgetFormFilterInput(),
      'caracteristicas_transmision'                  => new sfWidgetFormFilterInput(),
      'caracteristicas_contexto'                     => new sfWidgetFormFilterInput(),
      'caracteristicas_importancia_vida_social'      => new sfWidgetFormFilterInput(),
      'caracteristicas_otras_informaciones'          => new sfWidgetFormFilterInput(),
      'temporalidad_id'                              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Temporalidad'), 'add_empty' => true)),
      'detalle_temporalidad'                         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'estado_id'                                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Estado'), 'add_empty' => true)),
      'sensibilidad_al_cambio_id'                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SensibilidadAlCambio'), 'add_empty' => true)),
      'amenazas_sobre_la_practica'                   => new sfWidgetFormFilterInput(),
      'amenazas_sobre_la_transmision'                => new sfWidgetFormFilterInput(),
      'comunidad_se_llama_a_si_misma'                => new sfWidgetFormFilterInput(),
      'comunidad_se_siente_parte_de'                 => new sfWidgetFormFilterInput(),
      'comunidad_puede_catalogarse_etnicamente'      => new sfWidgetFormFilterInput(),
      'comunidad_puede_catalogarse_por_pertenencia'  => new sfWidgetFormFilterInput(),
      'descripcion_quienes_tienen_conocimiento'      => new sfWidgetFormFilterInput(),
      'descripcion_quienes_practican'                => new sfWidgetFormFilterInput(),
      'descripcion_entre_quienes_se_transmite'       => new sfWidgetFormFilterInput(),
      'evaluacion_problemas_posibilidades'           => new sfWidgetFormFilterInput(),
      'evaluacion_recomendaciones'                   => new sfWidgetFormFilterInput(),
      'publicar'                                     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'                                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'                                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'proyecto_id'                                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Proyecto'), 'column' => 'id')),
      'localizacion_id'                              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Localizacion'), 'column' => 'id')),
      'denominacion'                                 => new sfValidatorPass(array('required' => false)),
      'observaciones'                                => new sfValidatorPass(array('required' => false)),
      'area_expresion_id'                            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('AreaExpresion'), 'column' => 'id')),
      'nombre_en_otra_area'                          => new sfValidatorPass(array('required' => false)),
      'caracteristicas_resumen'                      => new sfValidatorPass(array('required' => false)),
      'caracteristicas_descripcion'                  => new sfValidatorPass(array('required' => false)),
      'caracteristicas_permanencia_transformaciones' => new sfValidatorPass(array('required' => false)),
      'caracteristicas_transmision'                  => new sfValidatorPass(array('required' => false)),
      'caracteristicas_contexto'                     => new sfValidatorPass(array('required' => false)),
      'caracteristicas_importancia_vida_social'      => new sfValidatorPass(array('required' => false)),
      'caracteristicas_otras_informaciones'          => new sfValidatorPass(array('required' => false)),
      'temporalidad_id'                              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Temporalidad'), 'column' => 'id')),
      'detalle_temporalidad'                         => new sfValidatorPass(array('required' => false)),
      'estado_id'                                    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Estado'), 'column' => 'id')),
      'sensibilidad_al_cambio_id'                    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SensibilidadAlCambio'), 'column' => 'id')),
      'amenazas_sobre_la_practica'                   => new sfValidatorPass(array('required' => false)),
      'amenazas_sobre_la_transmision'                => new sfValidatorPass(array('required' => false)),
      'comunidad_se_llama_a_si_misma'                => new sfValidatorPass(array('required' => false)),
      'comunidad_se_siente_parte_de'                 => new sfValidatorPass(array('required' => false)),
      'comunidad_puede_catalogarse_etnicamente'      => new sfValidatorPass(array('required' => false)),
      'comunidad_puede_catalogarse_por_pertenencia'  => new sfValidatorPass(array('required' => false)),
      'descripcion_quienes_tienen_conocimiento'      => new sfValidatorPass(array('required' => false)),
      'descripcion_quienes_practican'                => new sfValidatorPass(array('required' => false)),
      'descripcion_entre_quienes_se_transmite'       => new sfValidatorPass(array('required' => false)),
      'evaluacion_problemas_posibilidades'           => new sfValidatorPass(array('required' => false)),
      'evaluacion_recomendaciones'                   => new sfValidatorPass(array('required' => false)),
      'publicar'                                     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'                                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'                                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Creator'), 'column' => 'id')),
      'updated_by'                                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Updator'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('manifestacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Manifestacion';
  }

  public function getFields()
  {
    return array(
      'id'                                           => 'Number',
      'proyecto_id'                                  => 'ForeignKey',
      'localizacion_id'                              => 'ForeignKey',
      'denominacion'                                 => 'Text',
      'observaciones'                                => 'Text',
      'area_expresion_id'                            => 'ForeignKey',
      'nombre_en_otra_area'                          => 'Text',
      'caracteristicas_resumen'                      => 'Text',
      'caracteristicas_descripcion'                  => 'Text',
      'caracteristicas_permanencia_transformaciones' => 'Text',
      'caracteristicas_transmision'                  => 'Text',
      'caracteristicas_contexto'                     => 'Text',
      'caracteristicas_importancia_vida_social'      => 'Text',
      'caracteristicas_otras_informaciones'          => 'Text',
      'temporalidad_id'                              => 'ForeignKey',
      'detalle_temporalidad'                         => 'Text',
      'estado_id'                                    => 'ForeignKey',
      'sensibilidad_al_cambio_id'                    => 'ForeignKey',
      'amenazas_sobre_la_practica'                   => 'Text',
      'amenazas_sobre_la_transmision'                => 'Text',
      'comunidad_se_llama_a_si_misma'                => 'Text',
      'comunidad_se_siente_parte_de'                 => 'Text',
      'comunidad_puede_catalogarse_etnicamente'      => 'Text',
      'comunidad_puede_catalogarse_por_pertenencia'  => 'Text',
      'descripcion_quienes_tienen_conocimiento'      => 'Text',
      'descripcion_quienes_practican'                => 'Text',
      'descripcion_entre_quienes_se_transmite'       => 'Text',
      'evaluacion_problemas_posibilidades'           => 'Text',
      'evaluacion_recomendaciones'                   => 'Text',
      'publicar'                                     => 'Boolean',
      'created_at'                                   => 'Date',
      'updated_at'                                   => 'Date',
      'created_by'                                   => 'ForeignKey',
      'updated_by'                                   => 'ForeignKey',
    );
  }
}
