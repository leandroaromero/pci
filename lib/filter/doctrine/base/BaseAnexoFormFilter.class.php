<?php

/**
 * Anexo filter form base class.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseAnexoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'manifestacion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'), 'add_empty' => true)),
      'nombre'           => new sfWidgetFormFilterInput(),
      'descripcion'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'manifestacion_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Manifestacion'), 'column' => 'id')),
      'nombre'           => new sfValidatorPass(array('required' => false)),
      'descripcion'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('anexo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Anexo';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'manifestacion_id' => 'ForeignKey',
      'nombre'           => 'Text',
      'descripcion'      => 'Text',
    );
  }
}
