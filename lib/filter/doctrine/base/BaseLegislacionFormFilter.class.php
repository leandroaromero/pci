<?php

/**
 * Legislacion filter form base class.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseLegislacionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'tipo'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'numero'  => new sfWidgetFormFilterInput(),
      'titulo'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'resumen' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'tipo'    => new sfValidatorPass(array('required' => false)),
      'numero'  => new sfValidatorPass(array('required' => false)),
      'titulo'  => new sfValidatorPass(array('required' => false)),
      'resumen' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('legislacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Legislacion';
  }

  public function getFields()
  {
    return array(
      'id'      => 'Number',
      'tipo'    => 'Text',
      'numero'  => 'Text',
      'titulo'  => 'Text',
      'resumen' => 'Text',
    );
  }
}
