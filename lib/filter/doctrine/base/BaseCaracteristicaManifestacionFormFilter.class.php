<?php

/**
 * CaracteristicaManifestacion filter form base class.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCaracteristicaManifestacionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'manifestacion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'manifestacion_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Manifestacion'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('caracteristica_manifestacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CaracteristicaManifestacion';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'manifestacion_id' => 'ForeignKey',
    );
  }
}
