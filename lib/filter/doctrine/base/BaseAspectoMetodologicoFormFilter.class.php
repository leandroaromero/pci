<?php

/**
 * AspectoMetodologico filter form base class.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseAspectoMetodologicoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'manifestacion_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'), 'add_empty' => true)),
      'entidad_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'), 'add_empty' => true)),
      'criterios_modos'            => new sfWidgetFormFilterInput(),
      'relacion_con_manifestacion' => new sfWidgetFormFilterInput(),
      'nota_observacion'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'manifestacion_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Manifestacion'), 'column' => 'id')),
      'entidad_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Entidad'), 'column' => 'id')),
      'criterios_modos'            => new sfValidatorPass(array('required' => false)),
      'relacion_con_manifestacion' => new sfValidatorPass(array('required' => false)),
      'nota_observacion'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('aspecto_metodologico_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AspectoMetodologico';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'manifestacion_id'           => 'ForeignKey',
      'entidad_id'                 => 'ForeignKey',
      'criterios_modos'            => 'Text',
      'relacion_con_manifestacion' => 'Text',
      'nota_observacion'           => 'Text',
    );
  }
}
