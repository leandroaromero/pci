<?php

/**
 * Entidad filter form.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EntidadFormFilter extends BaseEntidadFormFilter
{
  public function configure()
  {

	  unset(
      	$this['updated_at'],        
      	$this['updated_by'],
  		$this['created_by'],
  		$this['created_at']
  	);
  }
}
