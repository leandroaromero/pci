<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Portador', 'doctrine');

/**
 * BasePortador
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $manifestacion_id
 * @property integer $entidad_id
 * @property Entidad $Entidad
 * @property Manifestacion $Manifestacion
 * 
 * @method integer       getManifestacion_id() Returns the current record's "manifestacion_id" value
 * @method integer       getEntidad_id()       Returns the current record's "entidad_id" value
 * @method Entidad       getEntidad()          Returns the current record's "Entidad" value
 * @method Manifestacion getManifestacion()    Returns the current record's "Manifestacion" value
 * @method Portador      setManifestacion_id() Sets the current record's "manifestacion_id" value
 * @method Portador      setEntidad_id()       Sets the current record's "entidad_id" value
 * @method Portador      setEntidad()          Sets the current record's "Entidad" value
 * @method Portador      setManifestacion()    Sets the current record's "Manifestacion" value
 * 
 * @package    pci
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePortador extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('portador');
        $this->hasColumn('manifestacion_id', 'integer', 10, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
             'length' => 10,
             ));
        $this->hasColumn('entidad_id', 'integer', 10, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
             'length' => 10,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Entidad', array(
             'local' => 'entidad_id',
             'foreign' => 'id'));

        $this->hasOne('Manifestacion', array(
             'local' => 'manifestacion_id',
             'foreign' => 'id'));
    }
}