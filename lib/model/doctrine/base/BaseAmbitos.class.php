<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Ambitos', 'doctrine');

/**
 * BaseAmbitos
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $nombre
 * @property integer $ambitos_id
 * @property Doctrine_Collection $ManifestacionAmbitos
 * 
 * @method integer             getId()                   Returns the current record's "id" value
 * @method string              getNombre()               Returns the current record's "nombre" value
 * @method integer             getAmbitos_id()           Returns the current record's "ambitos_id" value
 * @method Doctrine_Collection getManifestacionAmbitos() Returns the current record's "ManifestacionAmbitos" collection
 * @method Ambitos             setId()                   Sets the current record's "id" value
 * @method Ambitos             setNombre()               Sets the current record's "nombre" value
 * @method Ambitos             setAmbitos_id()           Sets the current record's "ambitos_id" value
 * @method Ambitos             setManifestacionAmbitos() Sets the current record's "ManifestacionAmbitos" collection
 * 
 * @package    pci
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseAmbitos extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('ambitos');
        $this->hasColumn('id', 'integer', 10, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 10,
             ));
        $this->hasColumn('nombre', 'string', 250, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 250,
             ));
        $this->hasColumn('ambitos_id', 'integer', 10, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 10,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('ManifestacionAmbitos', array(
             'local' => 'id',
             'foreign' => 'ambitos_id'));
        $this->hasOne('Ambitos', array(
             'local' => 'ambitos_id',
             'foreign' => 'id'));
    }
}