<?php

/**
 * AspectoMetodologico form base class.
 *
 * @method AspectoMetodologico getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAspectoMetodologicoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'manifestacion_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'), 'add_empty' => false)),
      'entidad_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'), 'add_empty' => false)),
      'criterios_modos'            => new sfWidgetFormTextarea(),
      'relacion_con_manifestacion' => new sfWidgetFormTextarea(),
      'nota_observacion'           => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'manifestacion_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'))),
      'entidad_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'))),
      'criterios_modos'            => new sfValidatorString(array('required' => false)),
      'relacion_con_manifestacion' => new sfValidatorString(array('required' => false)),
      'nota_observacion'           => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('aspecto_metodologico[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AspectoMetodologico';
  }

}
