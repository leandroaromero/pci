<?php

/**
 * ManifestacionAmbitos form base class.
 *
 * @method ManifestacionAmbitos getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseManifestacionAmbitosForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'manifestacion_id' => new sfWidgetFormInputHidden(),
      'ambitos_id'       => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'manifestacion_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('manifestacion_id')), 'empty_value' => $this->getObject()->get('manifestacion_id'), 'required' => false)),
      'ambitos_id'       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('ambitos_id')), 'empty_value' => $this->getObject()->get('ambitos_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('manifestacion_ambitos[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ManifestacionAmbitos';
  }

}
