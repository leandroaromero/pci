<?php

/**
 * Entidad form base class.
 *
 * @method Entidad getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseEntidadForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'tipo'            => new sfWidgetFormInputText(),
      'nombre'          => new sfWidgetFormInputText(),
      'edad_tiempo'     => new sfWidgetFormInputText(),
      'cargo'           => new sfWidgetFormInputText(),
      'direccion'       => new sfWidgetFormInputText(),
      'localizacion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localizacion'), 'add_empty' => false)),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'created_by'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'tipo'            => new sfValidatorString(array('max_length' => 50)),
      'nombre'          => new sfValidatorString(array('max_length' => 50)),
      'edad_tiempo'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'cargo'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'direccion'       => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'localizacion_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localizacion'))),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'created_by'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('entidad[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Entidad';
  }

}
