<?php

/**
 * ManifestacionLegislacion form base class.
 *
 * @method ManifestacionLegislacion getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseManifestacionLegislacionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'manifestacion_id' => new sfWidgetFormInputHidden(),
      'legislacion_id'   => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'manifestacion_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('manifestacion_id')), 'empty_value' => $this->getObject()->get('manifestacion_id'), 'required' => false)),
      'legislacion_id'   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('legislacion_id')), 'empty_value' => $this->getObject()->get('legislacion_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('manifestacion_legislacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ManifestacionLegislacion';
  }

}
