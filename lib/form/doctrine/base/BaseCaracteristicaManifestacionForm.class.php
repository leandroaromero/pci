<?php

/**
 * CaracteristicaManifestacion form base class.
 *
 * @method CaracteristicaManifestacion getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCaracteristicaManifestacionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'manifestacion_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'manifestacion_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Manifestacion'))),
    ));

    $this->widgetSchema->setNameFormat('caracteristica_manifestacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CaracteristicaManifestacion';
  }

}
