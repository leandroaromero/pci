<?php

/**
 * Legislacion form base class.
 *
 * @method Legislacion getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseLegislacionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'      => new sfWidgetFormInputHidden(),
      'tipo'    => new sfWidgetFormInputText(),
      'numero'  => new sfWidgetFormInputText(),
      'titulo'  => new sfWidgetFormInputText(),
      'resumen' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'tipo'    => new sfValidatorString(array('max_length' => 50)),
      'numero'  => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'titulo'  => new sfValidatorString(array('max_length' => 50)),
      'resumen' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('legislacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Legislacion';
  }

}
