<?php

/**
 * Manifestacion form base class.
 *
 * @method Manifestacion getObject() Returns the current form's model object
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseManifestacionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                           => new sfWidgetFormInputHidden(),
      'proyecto_id'                                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Proyecto'), 'add_empty' => false)),
      'localizacion_id'                              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localizacion'), 'add_empty' => false)),
      'denominacion'                                 => new sfWidgetFormInputText(),
      'observaciones'                                => new sfWidgetFormTextarea(),
      'area_expresion_id'                            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AreaExpresion'), 'add_empty' => true)),
      'nombre_en_otra_area'                          => new sfWidgetFormInputText(),
      'caracteristicas_resumen'                      => new sfWidgetFormTextarea(),
      'caracteristicas_descripcion'                  => new sfWidgetFormTextarea(),
      'caracteristicas_permanencia_transformaciones' => new sfWidgetFormTextarea(),
      'caracteristicas_transmision'                  => new sfWidgetFormTextarea(),
      'caracteristicas_contexto'                     => new sfWidgetFormTextarea(),
      'caracteristicas_importancia_vida_social'      => new sfWidgetFormTextarea(),
      'caracteristicas_otras_informaciones'          => new sfWidgetFormTextarea(),
      'temporalidad_id'                              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Temporalidad'), 'add_empty' => true)),
      'detalle_temporalidad'                         => new sfWidgetFormTextarea(),
      'estado_id'                                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Estado'), 'add_empty' => false)),
      'sensibilidad_al_cambio_id'                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SensibilidadAlCambio'), 'add_empty' => true)),
      'amenazas_sobre_la_practica'                   => new sfWidgetFormTextarea(),
      'amenazas_sobre_la_transmision'                => new sfWidgetFormTextarea(),
      'comunidad_se_llama_a_si_misma'                => new sfWidgetFormInputText(),
      'comunidad_se_siente_parte_de'                 => new sfWidgetFormInputText(),
      'comunidad_puede_catalogarse_etnicamente'      => new sfWidgetFormInputText(),
      'comunidad_puede_catalogarse_por_pertenencia'  => new sfWidgetFormInputText(),
      'descripcion_quienes_tienen_conocimiento'      => new sfWidgetFormInputText(),
      'descripcion_quienes_practican'                => new sfWidgetFormInputText(),
      'descripcion_entre_quienes_se_transmite'       => new sfWidgetFormInputText(),
      'evaluacion_problemas_posibilidades'           => new sfWidgetFormTextarea(),
      'evaluacion_recomendaciones'                   => new sfWidgetFormTextarea(),
      'publicar'                                     => new sfWidgetFormInputCheckbox(),
      'created_at'                                   => new sfWidgetFormDateTime(),
      'updated_at'                                   => new sfWidgetFormDateTime(),
      'created_by'                                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'                                           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'proyecto_id'                                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Proyecto'))),
      'localizacion_id'                              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localizacion'))),
      'denominacion'                                 => new sfValidatorString(array('max_length' => 250)),
      'observaciones'                                => new sfValidatorString(array('required' => false)),
      'area_expresion_id'                            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('AreaExpresion'), 'required' => false)),
      'nombre_en_otra_area'                          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'caracteristicas_resumen'                      => new sfValidatorString(),
      'caracteristicas_descripcion'                  => new sfValidatorString(array('required' => false)),
      'caracteristicas_permanencia_transformaciones' => new sfValidatorString(array('required' => false)),
      'caracteristicas_transmision'                  => new sfValidatorString(array('required' => false)),
      'caracteristicas_contexto'                     => new sfValidatorString(array('required' => false)),
      'caracteristicas_importancia_vida_social'      => new sfValidatorString(array('required' => false)),
      'caracteristicas_otras_informaciones'          => new sfValidatorString(array('required' => false)),
      'temporalidad_id'                              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Temporalidad'), 'required' => false)),
      'detalle_temporalidad'                         => new sfValidatorString(),
      'estado_id'                                    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Estado'))),
      'sensibilidad_al_cambio_id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SensibilidadAlCambio'), 'required' => false)),
      'amenazas_sobre_la_practica'                   => new sfValidatorString(array('required' => false)),
      'amenazas_sobre_la_transmision'                => new sfValidatorString(array('required' => false)),
      'comunidad_se_llama_a_si_misma'                => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'comunidad_se_siente_parte_de'                 => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'comunidad_puede_catalogarse_etnicamente'      => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'comunidad_puede_catalogarse_por_pertenencia'  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'descripcion_quienes_tienen_conocimiento'      => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'descripcion_quienes_practican'                => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'descripcion_entre_quienes_se_transmite'       => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'evaluacion_problemas_posibilidades'           => new sfValidatorString(array('required' => false)),
      'evaluacion_recomendaciones'                   => new sfValidatorString(array('required' => false)),
      'publicar'                                     => new sfValidatorBoolean(array('required' => false)),
      'created_at'                                   => new sfValidatorDateTime(),
      'updated_at'                                   => new sfValidatorDateTime(),
      'created_by'                                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'                                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('manifestacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Manifestacion';
  }

}
