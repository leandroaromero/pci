<?php

/**
 * Ambitos form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AmbitosForm extends BaseAmbitosForm
{
  public function configure()
  {
  //	$this->widgetSchema['ambitos_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Ambitos'), 'add_empty' => true));

  	    $this->widgetSchema['ambitos_id'] = new sfWidgetFormDoctrineChoice(array(
        'model'   => 'Ambitos',
        'add_empty' => true
    ));
  }


  
}
