<?php

/**
 * Proyecto form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProyectoForm extends BaseProyectoForm
{
  public function configure()
  {

        $rango = range(2012, 2020);
        $arreglo_rango = array_combine($rango, $rango);
        $this->widgetSchema['fecha_inicio'] = new sfWidgetFormJQueryDate(array(
           'label' => 'Fecha de inicio*',
           'image'  => '/pci/web/images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))
        ));
  }
}
