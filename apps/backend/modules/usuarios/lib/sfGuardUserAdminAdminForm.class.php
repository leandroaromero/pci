<?php

/**
 * sfGuardUserAdminForm for admin generators
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUserAdminForm.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class sfGuardUserAdminAdminForm extends BasesfGuardUserAdminForm
{
  /**
   * @see sfForm
   */
  public function configure()
  {

   #  unset($this['groups_list'],$this['permissions_list'],$this['is_active']);

   # $this->widgetSchema['groups_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
    $this->widgetSchema['permissions_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
   # $this->widgetSchema['groups_list']->setLabel('Grupos');
    $this->widgetSchema['permissions_list']->setLabel('Permisos');
    #$this->widgetSchema['permissions_list']->setOption('table_method', 'getOrderNombre');

  }
}
