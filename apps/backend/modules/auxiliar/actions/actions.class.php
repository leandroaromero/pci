<?php

/**
 * auxiliar actions.
 *
 * @package    pci
 * @subpackage auxiliar
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class auxiliarActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }

   public function executeGetEntidades($request)
	{
	    $this->getResponse()->setContentType('application/json');
	    $entidades = Doctrine::getTable('Entidad')->retrieveForSelect(
	                $request->getParameter('q'),
	                $request->getParameter('limit')
	    );
	    return $this->renderText(json_encode($entidades));
	}

  public function executeGetLegislacion($request)
  {
      $this->getResponse()->setContentType('application/json');
      $legislaciones = Doctrine::getTable('Legislacion')->retrieveForSelect(
                  $request->getParameter('q'),
                  $request->getParameter('limit')
      );
      return $this->renderText(json_encode($legislaciones));
  }
}
