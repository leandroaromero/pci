<?php use_helper('I18N') ?>

<div id="sf_admin_container">    
    <?php if ($sf_user->hasFlash('notice')): ?>
      <div class="notice"><?php echo __($sf_user->getFlash('notice'), array(), 'sf_guard') ?></div>
    <?php endif; ?>

    <?php if ($sf_user->hasFlash('error')): ?>
      <div class="error"><?php echo __($sf_user->getFlash('error'), array(), 'sf_guard') ?></div>
    <?php endif; ?>
</div>

<div id="ctr" align="center">
  <div class="login">
    <div class="login-form">
        <form action="<?php echo url_for('@sf_guard_forgot_password_change?unique_key='.$sf_request->getParameter('unique_key')) ?>" method="POST">
            <p><h2><?php echo __('Hello %name%', array('%name%' => $user->getName()), 'sf_guard') ?></h2></p>
            <div class="form-block"> 
                <?php echo $form->renderGlobalErrors(); ?>
                <?php if(isset($form['_csrf_token'])): ?>
                    <?php echo $form['_csrf_token']->render(); ?> 
		<?php endif; ?>   
                <?php echo $form['id']->render(); ?> 
                <div class="inputlabel"><?php echo $form['password']->renderLabel(__('Password', array(), 'sf_guard')); ?>:</div>
                <div>
                    <?php echo $form['password']->renderError(); ?>
                    <?php echo $form['password']->render(array('class' => 'inputbox')); ?>
                </div>
                <div class="inputlabel"><?php echo $form['password_again']->renderLabel(__('Password again', array(), 'sf_guard')); ?>:</div>
                <div>
                    <?php echo $form['password_again']->renderError(); ?>
                    <?php echo $form['password_again']->render(array('class' => 'inputbox')); ?>
                </div>
                
                <div align="left"><input type="submit" name="submit" class="button clr" value="<?php echo __('Send'); ?>" /></div>
            </div>
        </form>
    </div>    
    <div class="login-text">
        <div class="ctr"><img alt="Security" src="<?php echo image_path(sfAdminDash::getProperty('web_dir', '/sfAdminDashPlugin').'/images/medium/reload_f2.png'); ?>" /></div>
        <p><?php echo __('Enter your new password in the form below.', null, 'sf_guard') ?></p>
    </div>

    <div class="clr"></div>
  </div>
</div>
