<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>


<center>
  <table class="nuevos_objetos">
    <tfoot>
        <td colspan="2"></td>
    </tfoot>
    <tbody>
    	<?php echo $form['lista_objetos_digitales'][$num]['archivo']->renderRow();?>
    	<?php echo $form['lista_objetos_digitales'][$num]['descripcion']->renderRow();?>
    	<?php echo $form['lista_objetos_digitales'][$num]['autor']->renderRow();?>
    	<?php echo $form['lista_objetos_digitales'][$num]['fecha']->renderRow();?>
    	<?php echo $form['lista_objetos_digitales'][$num]['objeto_principal']->renderRow();?>
    	
    </tbody>    
  </table>  
</center>  
