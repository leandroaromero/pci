
      <script>
        $(function() 
          {      
            $( "#tabs" ).tabs();   
          });     
      </script>

  <style>
    #tabs{
      font: 90.5% "Trebuchet MS", sans-serif;
    }
  </style>



<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>



<div class="sf_admin_form">
  <?php echo form_tag_for($form, '@manifestacion') ?>
  <?php echo $form->renderHiddenFields(false) ?>
  <?php if ($form->hasGlobalErrors()): ?>
    <?php echo $form->renderGlobalErrors() ?>
  <?php endif; ?>

  <div id="tabs">  
    <ul> 
      <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
        <li>
          <a href="#<?php echo $fieldset ?>"><?php echo $fieldset ?></a>
        </li>
      <?php endforeach; ?>
    </ul>    

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
    <div id="<?php echo $fieldset ?>">
      <?php include_partial('manifestacion/form_fieldset', array('manifestacion' => $manifestacion, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    </div>
    <?php endforeach; ?>
  </div>
      <?php include_partial('manifestacion/form_actions', array('manifestacion' => $manifestacion, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  
  </form>
</div>
