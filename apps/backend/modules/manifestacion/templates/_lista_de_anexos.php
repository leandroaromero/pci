<div class="sf_admin_form_row">

    <div>
        <label> Respecto </label>
    </div>    

    <div class="content">
        <table class="anexos">
        <?php  foreach ($form['lista_de_anexos'] as $key=>$f_portador ):?>
            
            <tfoot>
                <td colspan="3"></td>
            </tfoot>
            <thead>
              <th><?php echo $f_portador['nombre']->renderLabel(); ?></th>
              <th><?php echo $f_portador['descripcion']->renderLabel(); ?></th>
               <?php if (! $form->getObject()->isNew()):?>  
                  <th><?php echo $f_portador['delete']->renderLabel(); ?></th>
                <?php endif?>  
            </thead>
            <tbody>
              <tr >       
                 <td>
                    <?php echo $f_portador['nombre']->render(); ?>
                    <?php echo $f_portador['nombre']->renderError(); ?>
                 </td>
                 <td>
                    <?php echo $f_portador['id']->render(); ?>
                    <?php echo $f_portador['descripcion']->render(); ?>
                    <?php echo $f_portador['descripcion']->renderError(); ?>
                 </td>
                  <?php if (! $form->getObject()->isNew()):?>  
                    <td class="delete">
                      <?php echo $f_portador['delete']->render(); ?>
                      <?php echo $f_portador['delete']->renderError(); ?>
                    </td>         
                  <?php endif?>  
              </tr> 
            </tbody>    
        <?php endforeach;?>
        </table>  
    </div>
    <div class="help">
        
    </div>
</div>


