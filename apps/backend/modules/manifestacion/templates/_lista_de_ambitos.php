<div class="sf_admin_form_row">

    <div>
        <label> Ambitos </label>
    </div>    

    <div class="content">
        <table class="ambitos">

        <?php  foreach ($form['lista_de_ambitos'] as $key=>$f_ambitos ):?>            
            <tfoot>
                <td colspan="4"></td>
            </tfoot>
            <tbody>
              <tr >       
                <th>
                    <?php echo $f_ambitos['ambitos_id']->renderLabel(); ?>
                 </th>
                 <td>
                    <?php echo $f_ambitos['ambitos_id']->render(); ?>
                    <?php echo $f_ambitos['ambitos_id']->renderError(); ?>
                 </td>
                     <?php if (! $form->getObject()->isNew()):?>            
                 <th>
                    <?php echo $f_ambitos['delete']->renderLabel(); ?>
                 </th>
                 <td class="delete">
                    <?php echo $f_ambitos['delete']->render(); ?>
                    <?php echo $f_ambitos['delete']->renderError(); ?>
                 </td>         
               <?php endif?>
              </tr> 
            </tbody>    
        <?php endforeach;?>
        </table>  
    </div>
    <div class="help">
        
    </div>
</div>


