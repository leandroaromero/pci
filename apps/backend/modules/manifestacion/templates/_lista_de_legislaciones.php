<div class="sf_admin_form_row">

    <div>
        <label> Documentación </label>
    </div>    

    <div class="content">
        <table class="legislacion">
        <?php  foreach ($form['lista_de_legislaciones'] as $key=>$f_legislacion ):?>            
            <tfoot>
                <td colspan="4"></td>
            </tfoot>
            <tbody>
              <tr >       
                <th>
                    <?php echo $f_legislacion['legislacion_id']->renderLabel(); ?>
                 </th>
                 <td>
                    <?php echo $f_legislacion['legislacion_id']->render(); ?>
                    <?php echo $f_legislacion['legislacion_id']->renderError(); ?>
                 </td>     
                 <?php if (! $form->getObject()->isNew()):?>      
                    <th>
                      <?php echo $f_legislacion['delete']->renderLabel(); ?>
                    </th>
                    <td class="delete">
                      <?php echo $f_legislacion['delete']->render(); ?>
                      <?php echo $f_legislacion['delete']->renderError(); ?>
                    </td>         
                 <?php endif?>   
              </tr> 
            </tbody>    
        <?php endforeach;?>
        </table>  
    </div>
    <div class="help">
        
    </div>
</div>


