<div class="sf_admin_form_row">

    <div>
        <label> Obtuvo la información por medio de:</label>
    </div>    

    <div class="content">
        <table class="aspectos_metodologicos">
        <?php  foreach ($form['lista_aspectos_metodologicos'] as $key=>$f_metodo ):?>
            
            <tfoot>
                <td colspan="4"></td>
            </tfoot>
            <tbody>
              <tr >       
                <th>
                    <?php echo $f_metodo['entidad_id']->renderLabel(); ?>
                 </th>
                 <td>
                    <?php echo $f_metodo['entidad_id']->render(); ?>
                    <?php echo $f_metodo['entidad_id']->renderError(); ?>
                 </td>        
               </tr>
               <tr>   
                 <th>
                    <?php echo $f_metodo['criterios_modos']->renderLabel(); ?>
                 </th>
                 <td class="delete">
                    <?php echo $f_metodo['criterios_modos']->render(); ?>
                    <?php echo $f_metodo['criterios_modos']->renderError(); ?>
                    </br>
                    "Sintetizo la informacion"
                 </td>         
              </tr> 
               <tr>   
                 <th>
                    <?php echo $f_metodo['relacion_con_manifestacion']->renderLabel(); ?>
                 </th>
                 <td class="delete">
                    <?php echo $f_metodo['relacion_con_manifestacion']->render(); ?>
                    <?php echo $f_metodo['relacion_con_manifestacion']->renderError(); ?>
                 </td>         
              </tr> 
               <tr>   
                 <th>
                    <?php echo $f_metodo['nota_observacion']->renderLabel(); ?>
                 </th>
                 <td class="delete">
                    <?php echo $f_metodo['nota_observacion']->render(); ?>
                    <?php echo $f_metodo['nota_observacion']->renderError(); ?>
                 </td>         
              </tr>               
              <?php if (! $form->getObject()->isNew() && $f_metodo['id']->getValue() > 0):?>  
                <tr>     
                    <th>
                        <?php echo $f_metodo['delete']->renderLabel(); ?>
                    </th>
                    <td class="delete">
                        <?php echo $f_metodo['id']->render(); ?>
                        <?php echo $f_metodo['delete']->render(); ?>
                        <?php echo $f_metodo['delete']->renderError(); ?>
                    </td>    
                </tr>
            <?php endif?>
              
            </tbody>    
        <?php endforeach;?>
        </table>  
    </div>
    <div class="help">
        
    </div>
</div>

