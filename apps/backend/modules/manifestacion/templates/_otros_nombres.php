<div class="sf_admin_form_row">
    <div>
        <label> Otras denominaciones</label>
    </div>    

    <div class="content">

		<?php  foreach ($form->getObject()->getLocalizacion()->getOtrosnombresLocalizacion() as $otros ):?>
   			<?php echo $otros->getNombre() ?></br>
		<?php endforeach ?>
	</div>	
    <div class="help">
        Otros nombre por el cual se la conoce a la localización elegida 
    </div>
</div>	