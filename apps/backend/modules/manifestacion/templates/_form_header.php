

<script type="text/javascript">
var port = <?php print_r($form['lista_de_portadores']->count())?>;

function addPortador(num1) {
  var r2 = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('manifestacion/addPortadorForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return r2;
}
$().ready(function() {
  $('button#add_portador').click(function() {
    $("#extra_portador").append(addPortador(port));
    port = port + 1;
  });
});
</script>

<script type="text/javascript">
var leg = <?php print_r($form['lista_de_legislaciones']->count())?>;

function addLegislacion(num1) {
  var r2 = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('manifestacion/addLegislacionForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return r2;
}
$().ready(function() {
  $('button#add_legislacion').click(function() {
    $("#extra_legislacion").append(addLegislacion(leg));
    leg = leg + 1;
  });
});
</script>

<script type="text/javascript">
var anex= <?php print_r($form['lista_de_anexos']->count())?>;

function addAnexo(num1) {
  var r2 = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('manifestacion/addAnexoForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return r2;
}
$().ready(function() {
  $('button#add_anexo').click(function() {
    $("#extra_anexo").append(addAnexo(anex));
    anex = anex + 1;
  });
});
</script>


<script type="text/javascript">
var objd = <?php print_r($form['lista_objetos_digitales']->count())?>;

function addObjeto(num1) {
  var od = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('manifestacion/addObjetoForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return od;
}
$().ready(function() {
  $('button#add_objeto').click(function() {
    $("#extra_objeto").append(addObjeto(objd));
    objd = objd + 1;
  });
});
</script>




<script type="text/javascript">
var ambi = <?php print_r($form['lista_de_ambitos']->count())?>;

function addAmbitos(num1) {
  var r2 = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('manifestacion/addAmbitosForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return r2;
}
$().ready(function() {
  $('button#add_ambitos').click(function() {
    $("#extra_ambitos").append(addAmbitos(ambi));
    ambi = ambi + 1;
  });
});
</script>


<script type="text/javascript">
var met = <?php print_r($form['lista_aspectos_metodologicos']->count())?>;

function addMetodo(num1) {
  var m2 = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('manifestacion/addMetodoForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?num1=':'&num1=')?>'+num1,
    async: false
  }).responseText;
  return m2;
}
$().ready(function() {
  $('button#add_metodo').click(function() {
    $("#extra_metodo").append(addMetodo(met));
    met = met + 1;
  });
});
</script>