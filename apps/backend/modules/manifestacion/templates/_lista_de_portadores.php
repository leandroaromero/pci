<div class="sf_admin_form_row">

    <div>
        <label> Respecto de los Portadores </label>
    </div>    

    <div class="content">
        <table class="portadores_entidad">
        <?php  foreach ($form['lista_de_portadores'] as $key=>$f_portador ):?>
            
            <tfoot>
                <td colspan="4"></td>
            </tfoot>
            <tbody>
              <tr >       
                <th>
                    <?php echo $f_portador['entidad_id']->renderLabel(); ?>
                 </th>
                 <td>
                    <?php echo $f_portador['entidad_id']->render(); ?>
                    <?php echo $f_portador['entidad_id']->renderError(); ?>
                 </td>    
                <?php if (! $form->getObject()->isNew()):?>       
                  <th>
                    <?php echo $f_portador['delete']->renderLabel(); ?>
                  </th>
                  <td class="delete">
                    <?php echo $f_portador['delete']->render(); ?>
                    <?php echo $f_portador['delete']->renderError(); ?>
                  </td>         
                <?php endif?>  
              </tr> 
            </tbody>    
        <?php endforeach;?>
        </table>  
    </div>
    <div class="help">
        
    </div>
</div>


