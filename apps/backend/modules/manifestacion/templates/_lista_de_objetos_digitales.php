
<div class="sf_admin_form_row">

    <div>
        <label> Digitales </label>
    </div>    

    <div class="content">
        <table class="archivo_digital">
        <?php  foreach ($form['lista_objetos_digitales'] as $key=>$f_contacto ):?>
            
            <tfoot>
                <td colspan="3"></td>
            </tfoot>
            <tbody>
            <tr >       
                <th>
                    <?php echo $f_contacto['archivo']->renderLabel(); ?>
                </th>
                <td>
                    <?php echo $f_contacto['archivo']->render(); ?>
                    <?php echo $f_contacto['archivo']->renderError(); ?>
                </td>
                <td rowspan="6">
                  <?php include_partial('manifestacion/objeto', array( 'key' => $f_contacto['id']->getValue())) ?>

                </td>

            </tr>
            <tr>             
                <th>
                    <?php echo $f_contacto['descripcion']->renderLabel(); ?>
                </th>
                <td colspan="">
                    <?php echo $f_contacto['descripcion']->render(); ?>
                    <?php echo $f_contacto['descripcion']->renderError(); ?>
                </td>
            </tr>
            <tr>    
                <th>
                    <?php echo $f_contacto['autor']->renderLabel(); ?>
                </th>
                <td>    
                    <?php echo $f_contacto['autor']->render(); ?>
                    <?php echo $f_contacto['autor']->renderError(); ?>
                </td>
            </tr>
            <tr>    
                <th>
                    <?php echo $f_contacto['fecha']->renderLabel(); ?>
                </th>
                <td>    
                    <?php echo $f_contacto['fecha']->render(); ?>
                    <?php echo $f_contacto['fecha']->renderError(); ?>
                </td>                
            </tr>
            <tr>    
                <th>
                    <?php echo $f_contacto['objeto_principal']->renderLabel(); ?>
                </th>
                <td class="delete" >    
                    <?php echo $f_contacto['objeto_principal']->render(); ?>
                    <?php echo $f_contacto['objeto_principal']->renderError(); ?>
                </td>
            </tr>
            <?php if (! $form->getObject()->isNew() && $f_contacto['id']->getValue() > 0):?>  
                <tr>     
                    <th>
                        <?php echo $f_contacto['delete']->renderLabel(); ?>
                    </th>
                    <td class="delete">
                        <?php echo $f_contacto['id']->render(); ?>
                        <?php echo $f_contacto['delete']->render(); ?>
                        <?php echo $f_contacto['delete']->renderError(); ?>
                    </td>    
                </tr>
            <?php endif?>
            </tbody>    
        <?php endforeach;?>
        </table>  
    </div>
    <div class="help">
       
    </div>
</div>


