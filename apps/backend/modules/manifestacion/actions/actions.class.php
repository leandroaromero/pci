<?php

require_once dirname(__FILE__).'/../lib/manifestacionGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/manifestacionGeneratorHelper.class.php';

/**
 * manifestacion actions.
 *
 * @package    pci
 * @subpackage manifestacion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class manifestacionActions extends autoManifestacionActions
{

	public function executeAddPortadorForm($request)
    {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($manifestacion = Doctrine::getTable('Manifestacion')->find($request->getParameter('id')))
         {  
             $form = new ManifestacionAdminForm($manifestacion);
             }else{
             	$form = new ManifestacionAdminForm(null);
         }
         $form->addPortador($number);
         return $this->renderPartial('addPortador',array('form' => $form, 'num' => $number));
    }   

	public function executeAddLegislacionForm($request)
    {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($manifestacion = Doctrine::getTable('Manifestacion')->find($request->getParameter('id')))
         {  
             $form = new ManifestacionAdminForm($manifestacion);
             }else{
             	$form = new ManifestacionAdminForm(null);
         }
         $form->addLegislacion($number);
         return $this->renderPartial('addLegislacion',array('form' => $form, 'num' => $number));
    }   

    public function executeAddAnexoForm($request)
    {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($manifestacion = Doctrine::getTable('Manifestacion')->find($request->getParameter('id')))
         {  
             $form = new ManifestacionAdminForm($manifestacion);
             }else{
                $form = new ManifestacionAdminForm(null);
         }
         $form->addAnexo($number);
         return $this->renderPartial('addAnexo',array('form' => $form, 'num' => $number));
    }   

    public function executeAddObjetoForm($request)
    {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($manifestacion = Doctrine::getTable('Manifestacion')->find($request->getParameter('id')))
         {  
             $form = new ManifestacionAdminForm($manifestacion);
             }else{
                $form = new ManifestacionAdminForm(null);
         }
         $form->addObjetoD($number);
         return $this->renderPartial('addObjeto',array('form' => $form, 'num' => $number));
    } 

    public function executeAddAmbitosForm($request)
    {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($manifestacion = Doctrine::getTable('Manifestacion')->find($request->getParameter('id')))
         {  
             $form = new ManifestacionAdminForm($manifestacion);
             }else{
                $form = new ManifestacionAdminForm(null);
         }
         $form->addAmbitos($number);
         return $this->renderPartial('addAmbitos',array('form' => $form, 'num' => $number));
    } 

    public function executeAddMetodoForm($request)
    {
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("num1"));

         if($manifestacion = Doctrine::getTable('Manifestacion')->find($request->getParameter('id')))
         {  
             $form = new ManifestacionAdminForm($manifestacion);
             }else{
                $form = new ManifestacionAdminForm(null);
         }
         $form->addMetodo($number);
         return $this->renderPartial('addMetodo',array('form' => $form, 'num' => $number));
    } 
}
