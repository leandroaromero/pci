<?php

/**
 * Anexo form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AnexoAdminForm extends BaseAnexoForm
{
  	public function configure()
  	{
	    unset( $this['manifestacion_id']);
  	
 	 	if ($this->object->exists())
    	{
      		$this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      		$this->validatorSchema['delete'] = new sfValidatorPass();
    	}
	}

}
