<?php

/**
 * ManifestacionAmbitos form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ManifestacionAmbitosAdminForm extends BaseManifestacionAmbitosForm
{
  public function configure()
  {
	  unset( $this['manifestacion_id'] );
	  $this->widgetSchema['ambitos_id'] = new sfWidgetFormDoctrineChoice(array(
        'model'        => 'Ambitos',
        'table_method' => 'getAmbitosOrdenados',
        'add_empty'    => true
    ));

	  $this->validatorSchema['ambitos_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Ambitos'), 'required' => false));	  

    $this->widgetSchema['ambitos_id']->setAttributes(array('style' =>'width:500px'));
   

	 if ($this->object->exists())
    	{
      		$this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      		$this->validatorSchema['delete'] = new sfValidatorPass();
    	}
  }
}
