<?php

/**
 * AspectoMetodologico form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AspectoMetodologicoAdminForm extends BaseAspectoMetodologicoForm
{
  public function configure()
  {
	unset( $this['manifestacion_id']);

  	sfProjectConfiguration::getActive()->loadHelpers('Url');
    $this->widgetSchema['entidad_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'), 'add_empty' => true));
    $this->widgetSchema['entidad_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
    $this->widgetSchema['entidad_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'label' => 'Persona/Grupo de Personas*',
        'model' => "Entidad",
        'url'   => url_for("@ajax_entidades"),
        'config' => '{ max: 30}',
        'method' => 'getPortadores'   
        ));
    $this->widgetSchema['entidad_id']->setAttributes(array('style' => 'font-weight:bold !important')); 
    $this->widgetSchema['criterios_modos']->setOption('label', 'Los criterios y el modo como sintetizó la información fueron 
');
    $this->widgetSchema['criterios_modos']->setOption('label', 'Criterios/Modo');

    //$this->widgetSchema['criterios_modos']->setOption('help', 'sfWidgetFormDoctrineJQueryAutocompleter');
   // $this->widgetSchema['criterios_modos']->setAttributes(array('help' =>'Indica si el registro esta apto para el público en general'));

    $this->validatorSchema['entidad_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'), 'required' => false));

    if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }

  }
}
