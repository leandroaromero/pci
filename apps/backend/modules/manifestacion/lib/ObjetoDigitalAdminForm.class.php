<?php

/**
 * ObjetoDigital form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ObjetoDigitalAdminForm extends BaseObjetoDigitalForm
{
  public function configure()
  {
	    unset( $this['manifestacion_id'],
	           $this['updated_at'],
	    	     $this['created_at'],
	    	     $this['updated_by'],
	    	     $this['created_by']
	    );

	    $uploads= sfConfig::get('sf_upload_dir');
      $this->widgetSchema['archivo'] = new sfWidgetFormInputFileEditable(array(
               'label'     =>  'Archivo*',
               'file_src'  =>  $uploads.'/digital/'.$this->getObject()->getArchivo(),
               'is_image'  =>  true,
               'edit_mode' =>  true,
               'template'  =>  "%input%",
            ));
      $this->validatorSchema['archivo'] = new sfValidatorFile(array(
               'required'   => false,
               'mime_types' => array( 'application/pdf',
                                      'application/x-pdf',
                                      
                                      'image/png',
                                      'image/jpg',       
                                      'image/jpeg',
                                      'image/pjpeg',
                                      'image/x-png',
                                      'image/gif',
                                      'image/bmp',
                                      'image/tiff',
                                      'image/x-icon',

                                      'video/avi',
                                      'video/divx',
                                      'video/x-flv',
                                      'video/quicktime',
                                      'video/mpeg',
                                      'video/mp4',
                                      'video/ogg',


                                      'audio/mpeg',
                                      'audio/x-realaudio',
                                      'audio/wav',
                                      'audio/ogg',
                                      'audio/midi',
                                      'audio/x-ms-wma',
                                      'audio/x-ms-wax',
                                      'audio/x-matroska'
                                    ), 
                  'path' => $uploads.'/digital/',
          //     'max_size' => '8M'
               //'validated_file_class' => 'sfResizedFile',
            ));   
    
    if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  }
}
