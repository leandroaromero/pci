<?php

/**
 * ManifestacionLegislacion form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ManifestacionLegislacionAdminForm extends BaseManifestacionLegislacionForm
{
  public function configure()
  {
	    unset( $this['manifestacion_id']);


  	sfProjectConfiguration::getActive()->loadHelpers('Url');
    $this->widgetSchema['legislacion_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Legislacion'), 'add_empty' => true));
    $this->widgetSchema['legislacion_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
    $this->widgetSchema['legislacion_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Legislacion",
        'url'   => url_for("@ajax_legislacion"),
        'config' => '{ max: 30}',
        'method' => 'getEncabezado'
        ));

    $this->widgetSchema['legislacion_id']->setAttributes(array('style' =>'width:500px'));     
    $this->validatorSchema['legislacion_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Legislacion'), 'required' => false));
    if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  }
}
