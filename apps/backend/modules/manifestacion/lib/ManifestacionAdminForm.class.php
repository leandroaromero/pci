<?php

/**
 * Manifestacion form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ManifestacionAdminForm extends BaseManifestacionForm
{

  protected $scheduledForDeletion = array();  
  protected $scheduledForDeletionL = array();  
  protected $scheduledForDeletionA = array();  
  protected $scheduledForDeletionOD = array();  
  protected $scheduledForDeletionAS = array();  
  protected $scheduledForDeletionM = array();  

  public function configure()
  {
	  unset(
      $this['updated_at'],        
      $this['updated_by'],
  		$this['created_by'],
  		$this['created_at']
  	);

    $this->setTitles();
      //Empotramos al menos un formulario de Portadores
      $portadores = $this->getObject()->getPortador();
      if (!$this->getObject()->getPortador()->count())
      {
        $portador = new Portador();
        $portador->setManifestacion($this->getObject());
      //  $portadores = array($portador);
      }  
      //Un formulario vacío hará de contenedor para todas los anexos        
      $portadores_forms = new SfForm();
      $count = 0;
      foreach ($portadores as $portador) 
      {
        $portador_form = new PortadorAdminForm($portador);            
      //Empotramos cada formulario en el contenedor
      $portadores_forms->embedForm($count, $portador_form);
       $count ++;
    }
    //Empotramos el contenedor en el formulario principal
    $this->embedForm('lista_de_portadores', $portadores_forms);  



    //Empotramos al menos un formulario de ASPECTOS METODOLOGICOS
    $aspectos = $this->getObject()->getAspectoMetodologico();
    if (!$this->getObject()->getAspectoMetodologico()->count())
    {
      $aspecto = new AspectoMetodologico();
      $aspecto->setManifestacion($this->getObject());
      $aspectos = array($aspecto);
    }  
    //Un formulario vacío hará de contenedor para todas los anexos        
    $aspectos_forms = new SfForm();
    $count = 0;
    foreach ($aspectos as $aspecto) 
    {
      $aspecto_form = new AspectoMetodologicoAdminForm($aspecto);            
      //Empotramos cada formulario en el contenedor
      $aspectos_forms->embedForm($count, $aspecto_form);
       $count ++;
    }
    //Empotramos el contenedor en el formulario principal
    $this->embedForm('lista_aspectos_metodologicos', $aspectos_forms); 




       //Empotramos al menos un formulario de Legislacion

      $legislaciones = $this->getObject()->getManifestacionLegislacion();
      if (!$this->getObject()->getManifestacionLegislacion()->count())
      {
        $legislacion = new ManifestacionLegislacion();
        $legislacion->setManifestacion($this->getObject());
       // $legislaciones = array($legislacion);
      }  
      //Un formulario vacío hará de contenedor para todas los anexos        
      $legislaciones_forms = new SfForm();
      $count = 0;
      foreach ($legislaciones as $legislacion) 
      {
        $legislacion_form = new ManifestacionLegislacionAdminForm($legislacion);            
      //Empotramos cada formulario en el contenedor
      $legislaciones_forms->embedForm($count, $legislacion_form);
       $count ++;
    }
    //Empotramos el contenedor en el formulario principal
    $this->embedForm('lista_de_legislaciones', $legislaciones_forms);

    //Empotramos al menos un formulario de Anexos
    $anexos = $this->getObject()->getAnexo();
    if (!$this->getObject()->getAnexo()->count())
    {
      $anexo = new Anexo();
      $anexo->setManifestacion($this->getObject());
     // $anexos = array($anexo);
    }  
      //Un formulario vacío hará de contenedor para todas los anexos        
    $anexos_forms = new SfForm();
    $count = 0;
    foreach ($anexos as $anexo) 
    {
      $anex_form = new AnexoAdminForm($anexo);            
      //Empotramos cada formulario en el contenedor
      $anexos_forms->embedForm($count, $anex_form);
       $count ++;
    }
    //Empotramos el contenedor en el formulario principal
    $this->embedForm('lista_de_anexos', $anexos_forms);
      

    //Empotramos al menos un formulario de OBJETOS DIGITALES
      $imagenes = $this->getObject()->getObjetoDigital();
      if (!$this->getObject()->getObjetoDigital()->count()){
        $imagen = new ObjetoDigital();
        $imagen->setManifestacion($this->getObject());
        $imagenes = array($imagen);
      }  
        //Un formulario vacío hará de contenedor para todas las imagenes        
        $imagenes_forms = new SfForm();
        $countI = 0;
        foreach ($imagenes as $imagen) {
            $ima_form = new ObjetoDigitalAdminForm($imagen);            
                //Empotramos cada formulario en el contenedor
                $imagenes_forms->embedForm($countI, $ima_form);
                $countI ++;
            }
        //Empotramos el contenedor en el formulario principal
        $this->embedForm('lista_objetos_digitales', $imagenes_forms);



      //Empotramos al menos un formulario de Ambitos
      $ambitos = $this->getObject()->getManifestacionAmbitos();
      if (!$this->getObject()->getManifestacionAmbitos()->count())
      {
        $ambito = new ManifestacionAmbitos();
        $ambito->setManifestacion($this->getObject());
      //  $ambitos = array($ambito);
      }  
      //Un formulario vacío hará de contenedor para todas los anexos        
      $ambitos_forms = new SfForm();
      $count = 0;
      foreach ($ambitos as $ambito) 
      {
        $ambito_form = new ManifestacionAmbitosAdminForm($ambito);            
      //Empotramos cada formulario en el contenedor
      $ambitos_forms->embedForm($count, $ambito_form);
       $count ++;
    }
    //Empotramos el contenedor en el formulario principal
    $this->embedForm('lista_de_ambitos', $ambitos_forms);
      
      


      
  }

  public function bind(array $taintedValues = null, array $taintedFiles = null)
  {          
    foreach($taintedValues['lista_de_portadores'] as $key=>$porValues)
      {
        if (!isset($this['lista_de_portadores'][$key]) )
          {
            if ('' === trim($porValues['entidad_id']))
            {
              unset($values['lista_de_portadores'][$key]);
            }
             else
                {
                  $this->addPortador($key);
                }
          }
      }      
    foreach($taintedValues['lista_de_legislaciones'] as $key=>$porValues)
      {
        if (!isset($this['lista_de_legislaciones'][$key]) )
          {
            if ('' === trim($porValues['legislacion_id']))
            {
              unset($values['lista_de_legislaciones'][$key]);
            }
             else
                {
                  $this->addLegislacion($key);
                }
          }
      }      
    foreach($taintedValues['lista_de_anexos'] as $key=>$porValues)
      {
        if (!isset($this['lista_de_anexos'][$key]) )
          {
            if ('' === trim($porValues['nombre']))
            {
              unset($values['lista_de_anexos'][$key]);
            }
             else
                {
                  $this->addAnexo($key);
                }
          }
      }      
   

    foreach($taintedValues['lista_objetos_digitales'] as $key=>$porValues) 
    {
      if(false === $this->embeddedForms['lista_objetos_digitales']->offsetExists($key))
        {
          $this->addObjetoD($key);
        }
    }

    foreach($taintedValues['lista_de_ambitos'] as $key=>$porValues)
      {
        if (!isset($this['lista_de_ambitos'][$key]) )
          {
            if ($porValues['ambitos_id'] > 0 ){
                  $this->addAmbitos($key);
              }else{
                if ('' === trim($porValues['ambitos_id']) )
                  {
                    unset($taintedValues['lista_de_ambitos'][$key]);
                  }else
                      {
                        $this->addAmbitos($key);
                          }
              }
          }
      } 

    foreach($taintedValues['lista_aspectos_metodologicos'] as $key=>$porValues) 
    {
      if(false === $this->embeddedForms['lista_aspectos_metodologicos']->offsetExists($key))
        {
          if ('' === trim($porValues['entidad_id']))
            {
              unset($taintedValues['lista_aspectos_metodologicos'][$key]);
            }
             else
                {
                  $this->addMetodo($key);
                }  
        }
    }
    
    parent::bind($taintedValues, $taintedFiles);
  }





  protected function doBind(array $values)
  {
    foreach ($values['lista_de_portadores'] as $i => $porValues)
    {
      if ('' === trim($porValues['entidad_id']))
        {
           unset($values['lista_de_portadores'][$i]);
          }
          if (isset($porValues['delete']))
            {
              $this->scheduledForDeletion[$i] = $porValues['entidad_id'];
              unset($values['lista_de_portadores'][$i]);
             }
    }
    foreach ($values['lista_de_legislaciones'] as $i => $porValues)
    {
      if ('' === trim($porValues['legislacion_id']))
        {
           unset($values['lista_de_legislaciones'][$i]);
          }
      if (isset($porValues['delete']))
        {
          $this->scheduledForDeletionL[$i] = $porValues['legislacion_id'];
          unset($values['lista_de_legislaciones'][$i]);
        }
    }
    foreach ($values['lista_de_anexos'] as $i => $porValues)
    {
      if ('' === trim($porValues['nombre']))
        {
           unset($values['lista_de_anexos'][$i]);
          }
      if (isset($porValues['delete']))
        {
          $this->scheduledForDeletionA[$i] = $porValues['id'];
          unset($values['lista_de_anexos'][$i]);
        }
    }
    
    foreach ($values['lista_objetos_digitales'] as $i => $porValues)
    {
      if (isset($porValues['delete']))
        {
          $this->scheduledForDeletionOD[$i] = $porValues['id'];
          unset($values['lista_objetos_digitales'][$i]);
        }
    }

    foreach ($values['lista_de_ambitos'] as $i => $porValues)
    {
      if (isset($porValues['delete']))
        {
          $this->scheduledForDeletionAS[$i] = $porValues['ambitos_id'];
          unset($values['lista_de_ambitos'][$i]);
        }
    }

    foreach ($values['lista_aspectos_metodologicos'] as $i => $porValues)
    {
      if (isset($porValues['delete']))
        {
          $this->scheduledForDeletionM[$i] = $porValues['id'];
          unset($values['lista_aspectos_metodologicos'][$i]);
        }
    }


    parent::doBind($values);
  }

  protected function doUpdateObject($values)
    {
      foreach ($this->scheduledForDeletion as $index => $id)
        {
            
          $q = Doctrine_Query::create()
              ->from('Portador p')
              ->where('p.entidad_id = ?', $id)
              ->andWhere('p.manifestacion_id =?', $this->getObject()->getId() )
              ->fetchOne();
              $q->delete();

          }
      foreach ($this->scheduledForDeletionL as $index => $id)
        {
            
          $q = Doctrine_Query::create()
              ->from('ManifestacionLegislacion m')
              ->where('m.legislacion_id = ?', $id)
              ->andWhere('m.manifestacion_id =?', $this->getObject()->getId() )
              ->fetchOne();
              $q->delete();

          }
      foreach ($this->scheduledForDeletionA as $index => $id)
        {
            
          $q = Doctrine_Query::create()
              ->from('Anexo a')
              ->where('a.id = ?', $id)
              ->fetchOne();
              $q->delete();

          }
      foreach ($this->scheduledForDeletionOD as $index => $id)
        {
            
          $q = Doctrine_Query::create()
              ->from('ObjetoDigital od')
              ->where('od.id = ?', $id)
              ->fetchOne();

          $filepath2 = sfConfig::get('sf_upload_dir').'/digital/'.$q->getArchivo();
          @unlink($filepath2);
          $q->delete();
          }
      foreach ($this->scheduledForDeletionAS as $index => $id)
        {
            
          $q = Doctrine_Query::create()
              ->from('ManifestacionAmbitos ma')
              ->where('ma.ambitos_id = ?', $id)
              ->andWhere('ma.manifestacion_id =?', $this->getObject()->getId() )
              ->fetchOne();
              $q->delete();

          }


      foreach ($this->scheduledForDeletionM as $index => $id)
        {
            
          $q = Doctrine_Query::create()
              ->from('AspectoMetodologico am')
              ->where('am.id = ?', $id)
              ->andWhere('am.manifestacion_id =?', $this->getObject()->getId() )
              ->fetchOne();
              $q->delete();

          }  

        $this->getObject()->fromArray($values);
    }


    public function addPortador($num)
    {
      $portador = new Portador();
      $portador->setManifestacion($this->getObject());
      $portador_form = new PortadorAdminForm($portador);

      unset($portador_form['manifestacion_id']);
        //Empotramos la nueva pícture en el contenedor
      $this->embeddedForms['lista_de_portadores']->embedForm($num, $portador_form);
        //Volvemos a empotrar el contenedor
      $this->embedForm('lista_de_portadores', $this->embeddedForms['lista_de_portadores']);
    } 

    public function addLegislacion($num)
    {
      $legislacion = new ManifestacionLegislacion();
      $legislacion->setManifestacion($this->getObject());
      $legislacion_form = new ManifestacionLegislacionAdminForm($legislacion);

      unset($legislacion_form['manifestacion_id']);
        //Empotramos la nueva pícture en el contenedor
      $this->embeddedForms['lista_de_legislaciones']->embedForm($num, $legislacion_form);
        //Volvemos a empotrar el contenedor
      $this->embedForm('lista_de_legislaciones', $this->embeddedForms['lista_de_legislaciones']);
    } 

    public function addAnexo($num)
    {
      $anexo = new Anexo();
      $anexo->setManifestacion($this->getObject());
      $anexo_form = new AnexoAdminForm($anexo);

      unset($anexo_form['manifestacion_id']);
        //Empotramos la nueva pícture en el contenedor
      $this->embeddedForms['lista_de_anexos']->embedForm($num, $anexo_form);
        //Volvemos a empotrar el contenedor
      $this->embedForm('lista_de_anexos', $this->embeddedForms['lista_de_anexos']);
    } 

    public function addObjetoD($numI)
    {
      $objetoDigital = new ObjetoDigital();
      $objetoDigital->setManifestacion($this->getObject());
      $objetoDigital_form = new ObjetoDigitalAdminForm($objetoDigital);

      unset($objetoDigital_form['manifestacion_id']);
        //Empotramos la nueva pícture en el contenedor
      $this->embeddedForms['lista_objetos_digitales']->embedForm($numI, $objetoDigital_form);
        //Volvemos a empotrar el contenedor
      $this->embedForm('lista_objetos_digitales', $this->embeddedForms['lista_objetos_digitales']);
    } 

    public function addAmbitos($num)
    {
      $ambito = new ManifestacionAmbitos();
      $ambito->setManifestacion($this->getObject());
      $ambito_form = new ManifestacionAmbitosAdminForm($ambito);

      unset($ambito_form['manifestacion_id']);
        //Empotramos la nueva pícture en el contenedor
      $this->embeddedForms['lista_de_ambitos']->embedForm($num, $ambito_form);
        //Volvemos a empotrar el contenedor
      $this->embedForm('lista_de_ambitos', $this->embeddedForms['lista_de_ambitos']);
    }

    public function addMetodo($num)
    {
      $metodo = new AspectoMetodologico();
      $metodo->setManifestacion($this->getObject());
      $metodo_form = new AspectoMetodologicoAdminForm($metodo);

      unset($metodo_form['manifestacion_id']);
        //Empotramos la metodologia
      $this->embeddedForms['lista_aspectos_metodologicos']->embedForm($num, $metodo_form);
        //Volvemos a empotrar el contenedor
      $this->embedForm('lista_aspectos_metodologicos', $this->embeddedForms['lista_aspectos_metodologicos']);
    } 





protected function setTitles()
{
  $this->widgetSchema['proyecto_id']->setAttributes(array('title' =>
        'Si no encuentra el proyecto, deberá cargarlo en "Datos/Proyectos"'));

  $this->widgetSchema['localizacion_id']->setAttributes(array('title' =>
        'Si no encuentra la localización, deberá cargarlo en "Datos/Localizaciones"'));

  $this->widgetSchema['denominacion']->setAttributes(array('title' =>
        'Nombre en idioma de la comunidad. Si no lo tiene, repita del proyecto'));

  $this->widgetSchema['publicar']->setAttributes(array('title' =>
        'Indica si el registro esta apto para el público en general'));

#AMBITOS
  $this->widgetSchema['observaciones']->setAttributes(array('title' =>
        'Informacion que desee aportar para: aclaración de los ambitos a los que son asignados'));
#AREAS
  $this->widgetSchema['area_expresion_id']->setAttributes(array('title' =>
        'Si no encuentra el área de expresión, deberá cargarlo en "Datos/Areas de expresion'));

  $this->widgetSchema['nombre_en_otra_area']->setAttributes(array('title' =>
        'Indique el nombre con la que se la conoce en cada lugar, si es diferente a la indicada en el punto'));

#CARACTERISTICAS
  #caracteristicas_otras_informaciones
  $this->widgetSchema['caracteristicas_resumen']->setAttributes(array('title' =>
        'Resumen general de la manifestacion'));

  $this->widgetSchema['caracteristicas_descripcion']->setAttributes(array('title' =>
        '¿Cuáles son sus principales características?'));

  $this->widgetSchema['caracteristicas_permanencia_transformaciones']->setAttributes(array('title' =>
        '¿De dónde surge?. La comunidad traza el principio de la manifestación en ¿Qué transformaciones ha tenido? '));

  $this->widgetSchema['caracteristicas_transmision']->setAttributes(array('title' =>
        '¿Cómo se transmite? '));

  $this->widgetSchema['caracteristicas_contexto']->setAttributes(array('title' =>
        '¿Cómo se relaciona la manifestación con la comunidad, el medio y otras manifestaciones?'));

  $this->widgetSchema['caracteristicas_importancia_vida_social']->setAttributes(array('title' =>
        '¿De dónde surge?. La comunidad traza el principio de la manifestación en ¿Qué transformaciones ha tenido? '));

  #TEMPORALIDAD

  $this->widgetSchema['temporalidad_id']->setAttributes(array('title' =>
        'Si no encuentra la temporalidad deberá cargarlo en "Datos/Temporalidad"'));

  $this->widgetSchema['detalle_temporalidad']->setAttributes(array('title' =>
        ' ¿Existen fechas calendario en las que la manifestación se expresa?  SI / NO  ¿Cuáles? ¿La manifestación está asociada a una temporalidad particular? ¿A cuál? ¿La comunidad asocia la manifestación con una temporalidad particular? ¿Con cuál? '));
  #ESTADO
  # estado_id, sensibilidad_al_cambio_id, amenazas_sobre_la_practica, amenazas_sobre_la_transmision]
   
   /**       "Portadores": [ _lista_de_portadores, _agregarPortadores, comunidad_se_llama_a_si_misma, comunidad_se_siente_parte_de, comunidad_puede_catalogarse_etnicamente, comunidad_puede_catalogarse_por_pertenencia, descripcion_quienes_tienen_conocimiento, descripcion_quienes_practican, descripcion_entre_quienes_se_transmite]
          "Evaluacion": [evaluacion_problemas_posibilidades, evaluacion_recomendaciones]
          "Metodologia": [_lista_aspectos_metodologicos]
          "Legislacion": [ _lista_de_legislaciones, _agregarLegislacion ]
          "Anexos": [_lista_de_anexos, _agregarAnexo]
          "Objetos": [_lista_de_objetos_digitales , _agregarObjeto]   
*/
   
 
  }

}
