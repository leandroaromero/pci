<?php

/**
 * Portador form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PortadorAdminForm extends BasePortadorForm
{
  public function configure()
  {

    unset( $this['manifestacion_id']);

  	sfProjectConfiguration::getActive()->loadHelpers('Url');
    $this->widgetSchema['entidad_id'] =  new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'), 'add_empty' => true));
    $this->widgetSchema['entidad_id']->setOption('renderer_class', 'sfWidgetFormDoctrineJQueryAutocompleter');
    $this->widgetSchema['entidad_id'] = new sfWidgetFormDoctrineJQueryAutocompleter(
      array(
        'model' => "Entidad",
        'url'   => url_for("@ajax_entidades"),
        'config' => '{ max: 30}',
        'method' => 'getPortadores'
        ));
     
    $this->validatorSchema['entidad_id']=  new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Entidad'), 'required' => false));

    
    if ($this->object->exists())
    {
      $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
      $this->validatorSchema['delete'] = new sfValidatorPass();
    }
  }
}
