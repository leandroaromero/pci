<?php

/**
 * Entidad form.
 *
 * @package    pci
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class EntidadAdminForm extends BaseEntidadForm
{
  public function configure()
  {
	  unset(
        $this['updated_at'],        
        $this['updated_by'],
  		$this['created_by'],
  		$this['created_at']
  	);
	  	$choices = array(
  						'INDIVIDUO'   => 'INDIVIDUO',
 						'INSTITUCION' => 'INSTITUCION',
 						'GRUPO'       => 'GRUPO'
					);
 
		$this->widgetSchema['tipo'] = new sfWidgetFormChoice(array('choices' => $choices));

  }
}
