<?php use_helper('I18N') ?>

<form class="f-wrap-1" action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
  <table>
    <tbody>
         <tr>
          <td><?php echo $form['username']->renderLabel('Usuario') ?></td>
          <td>
              <?php echo $form['username']->render() ?>
              <?php echo $form['username']->renderError() ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $form['password']->renderLabel('Contraseña') ?></td>
          <td>
              <?php echo $form['password']->render() ?>
              <?php echo $form['password']->renderError() ?>
          </td>
        </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="2">
          <input type="submit"  class="f-submit" value="<?php echo __('Signin', null, 'sf_guard') ?>" />
          <?php echo $form->renderHiddenFields() ?>
          <?php $routes = $sf_context->getRouting()->getRoutes() ?>
          <?php if (isset($routes['sf_guard_forgot_password'])): ?>
            <a href="<?php echo url_for('@sf_guard_forgot_password') ?>"><?php echo __('Forgot your password?', null, 'sf_guard') ?></a>
          <?php endif; ?>

          <?php if (isset($routes['sf_guard_register'])): ?>
            &nbsp;&nbsp;&nbsp; <a href="<?php echo url_for('@sf_guard_register') ?>"><?php echo __('Want to register?', null, 'sf_guard') ?></a>
          <?php endif; ?>
        </td>
      </tr>
    </tfoot>
  </table>
</form>