<?php

/**
 * componentes actions.
 *
 * @package    pci
 * @subpackage componentes
 * @author     Leandro Antonio Romero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class componentesActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }


  //pasajes por el ajax

/*  public function executeGetManifestaciones($request)
  {
      $this->getResponse()->setContentType('application/json');

      $objetos = Doctrine::getTable('Manifestacion')->retrieveForSelect(
                  $request->getParameter('q'),
                  $request->getParameter('limit')
      );

      return $this->renderText(json_encode($objetos));
  }*/

  public function executeGetAmbitos($request)
  {
      $this->getResponse()->setContentType('application/json');
      
      $query = Doctrine::getTable('Ambitos')->retrieveSearchSelect(
                  $request->getParameter('ambitos'),
                  $request->getParameter('hijos')
            );


      $ambitos=array();
      foreach ($query as $key => $value) {
        $ambito=array();
        $ambito['id']=$value->getId();
        $ambito['nombre']=$value->getNombre();
        $ambito['padre'] =$value->getPadre();

        $ambitos[]=$ambito; 
      }

      return $this->renderText(json_encode( $ambitos));
  }



  public function executeGetManifestacion($request)
  {
      $this->getResponse()->setContentType('application/json');

      $this->forward404Unless($objeto = Doctrine_Core::getTable('Manifestacion')->find(array($request->getParameter('id'))), sprintf('Object public_object does not exist (%s).', $request->getParameter('id'))); 

      return $this->renderText(json_encode( $objeto->getArrayString()));
  }


  /*
        general = string de lo que escribio el usuario
        categorias =ambitos
        cantidad =cantidad de resultados por pagina
        pagina =pagina devuelta
*/
  public function executeGetManifestaciones($request)
  {
      $this->getResponse()->setContentType('application/json');

      $query = Doctrine::getTable('Manifestacion')->retrieveSearchSelect(
                  $request->getParameter('general'),
                  $request->getParameter('categorias')
      );

      $manifestaciones= $this->armarRespuesta($query,
                        $request->getParameter('cantidad', 10),
                        $request->getParameter('pagina', 1) 
      );
      return $this->renderText(json_encode($manifestaciones));
  }

  protected function armarRespuesta($query, $cantidad, $pagina)
  {
      $pager = new sfDoctrinePager( 'Manifestacion', $cantidad);
      $pager->setQuery($query);
      $pager->setPage($pagina);
      $pager->init();
      $dato = array();

      $dato['total']= $pager->getNbResults();
      if ($pager->haveToPaginate()): 
        $dato['pagina']=  $pager->getPage(); 
        $dato['pagina_total']=  $pager->getLastPage();
      endif; 

      $resultado= array();
      $resultado[]= $dato;
      foreach ($pager->getResults() as $key => $manifestacion) {
        $resultado[]= $manifestacion->getArrayString();
      }
      return $resultado;
  }


}
