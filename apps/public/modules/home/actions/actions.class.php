<?php

/**
 * home actions.
 *
 * @package    pci
 * @subpackage home
 * @author     Leandro Antonio Romero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    
  }

 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeDetalles(sfWebRequest $request)
  {
    
  }
  public function executeSetMenu($request) {
      $this->getUser()->setAttribute('menu',$request->getParameter('menu'));
  }
}
