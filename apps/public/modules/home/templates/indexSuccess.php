 <div class="wrapper bgded overlay" style="background-image:url('/pci/web/images/pescadores_1.jpg');">
        <div id="pageintro" class="hoc clear"> 
        <!-- ################################################################################################ -->
        <article>
          <h2 class="heading">¿Qué es el patrimonio cultural inmaterial?</h2>
          <p align="justify">
            Se entiende de este modo a los usos , representaciones, expresiones, conocimientos y técnicas -juntos a los instrumentos, objetos, artefactos y espacios culturales que les son inherentes- que las comunidades, los grupos y en algunos casos los individuos, reconozcan como para de su cultura.
            Es por ende, toda manifestación cultural asociada a significados colectivos compartidos y comunitarios
          </p>
          <footer><a class="btn" href="<?php echo url_for('home/detalles')?>">Leer mas ...</a></footer>
        </article>

    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="group">
      <article class="one_third first">
        <h6 class="heading font-x2">Manifestaciones Culturales</h6>
        <p align="justify">
          Es un medio de expresarse de una región determinada, puede ser por medio de danzas, canciones, música, artes, etc. Cada comunidad o pueblo tiene su propia manifestación folklórica
        </p>
        <p class="btmspace-30">Estas son las Manifestaciones Culturales de la Provincia del Chaco clasificadas según sus ambitos</p>
        <footer><a class="btn" href="<?php echo url_for('manifestacion/index')?>">Ver Todas &raquo;</a></footer>
      </article>



      <div class="two_third">
        <ul class="nospace group elements">
      

          <li class="one_half first">
            <article>
              <a href="<?php echo url_for('manifestacion/index?nombre=ARTES DE LA REPRESENTACION')?>">
                <i class="fa fa-language"></i>
              </a>
              <h6 class="heading">ARTES DE LA REPRESENTACION</h6>
           <!--   <p>Una Breves palabras sobre el arte de las representaciones [&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=ARTES DE LA REPRESENTACION')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>
          <li class="one_half">
            <article><a href="<?php echo url_for('manifestacion/index?nombre=CELEBRACIONES')?>"><i class="fa fa-star"></i></a>
              <h6 class="heading">CELEBRACIONES</h6>
              <!--  <p>Definiciones que serian necesarias [&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=CELEBRACIONES')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>


          <li class="one_half first" style="margin-bottom: 50px;">
            <article><a href="<?php echo url_for('manifestacion/index?nombre=CONOCIMIENTOS Y USOS')?>"><i class="fa fa-refresh"></i></a>
              <h6 class="heading">CONOCIMIENTOS Y USOS</h6>
              <!--  <p>Relacionados con N y U [&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=CONOCIMIENTOS Y USOS')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>
          <li class="one_half" style="margin-bottom: 50px;">
            <article><a href="<?php echo url_for('manifestacion/index?nombre=ESPACIOS SIMBÓLICOS  Y LUGARES  DE PRÁCTICAS SOCIALES')?>"><i class="fa fa-bank"></i></a>
              <h6 class="heading">ESPACIOS SIMBÓLICOS  Y LUGARES  DE PRÁCTICAS SOCIALES</h6>
              <!--  <p>Simbólicos de practicas sociales[&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=ESPACIOS SIMBÓLICOS  Y LUGARES  DE PRÁCTICAS SOCIALES')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>


          <li class="one_half first" style="margin-bottom: 50px;">
            <article><a href="<?php echo url_for('manifestacion/index?nombre=LENGUAS Y EXPRESIONES ORALES TRADICIONALES')?>"><i class="fa fa-comments"></i></a>
              <h6 class="heading">LENGUAS Y EXPRESIONES ORALES TRADICIONALES</h6>
              <!--  <p>Nunc porta vel tempus ex scelerisque donec gravida gravida faucibus maecenas dignissim [&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=LENGUAS Y EXPRESIONES ORALES TRADICIONALES')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>
          <li class="one_half" style="margin-bottom: 50px;">
            <article><a href="<?php echo url_for('manifestacion/index?nombre=TÉCNICAS ARTESANALES TRADICIONALES')?>"><i class="fa fa-american-sign-language-interpreting"></i></a>
              <h6 class="heading">TÉCNICAS ARTESANALES TRADICIONALES</h6>
              <!--  <p>Dui eget elit semper in lacinia arcu condimentum morbi vitae arcu pulvinar suscipit [&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=TÉCNICAS ARTESANALES TRADICIONALES')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>
          <li class="one_half first">
            <article><a href="<?php echo url_for('manifestacion/index?nombre=USOS SOCIALES')?>"><i class="fa fa-group"></i></a>
              <h6 class="heading">USOS SOCIALES</h6>
              <!--  <p>Nunc porta vel tempus ex scelerisque donec gravida gravida faucibus maecenas dignissim [&hellip;]</p>-->
              <footer><a href="<?php echo url_for('manifestacion/index?nombre=USOS SOCIALES')?>">Ver listado &raquo;</a></footer>
            </article>
          </li>

        </ul>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
