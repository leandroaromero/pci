<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/01.png');">
        <div id="pageintro" class="hoc clear"> 
        <!-- ################################################################################################ -->
        <article>
          <h2 class="heading">Información general</h2>
          <p align="justify">
			El Departamento de Patrimonio Inmaterial es el organismo del Instituto de Cultura responsable de organizar para el Estado chaqueño la identificación del Patrimonio Cultural Inmaterial (PCI) presente entre los habitantes de su territorio y llevar adelante la planificación y ejecución de políticas públicas de preservación y salvaguardia de los que así lo requieran.
		</p>
		<p align="justify">
			A través de él, se busca adherir a las políticas de la Unesco establecidas en la Convención para la Salvaguardia del Patrimonio Cultural Inmaterial (2003) en la que se establece que “cada Estado participante ha de elaborar en su territorio uno o varios inventarios que deben actualizarse de manera regular” (artículo 12).
		</p>
		<p align="justify">
			En el artículo 11 del apartado III de la misma, se fija como una de las Funciones de los Estados Partes el “identificar y definir los distintos elementos del patrimonio cultural inmaterial presentes en su territorio, con participación de las comunidades, los grupos y las organizaciones no gubernamentales pertinentes”.
		</p>
		<p align="justify">
			La intervención del Estado provincial debe orientarse a ayudar a las comunidades a acopiar y registrar información sobre los elementos de su PCI; transmitir conocimientos relativos al mismo por conductos más formales, como la enseñanza en el sistema educativo; promover la información sobre este patrimonio en los medios de información y comunicación y toda otra acción encaminada a la identificación, puesta en valor y salvaguarda del PCI en el territorio chaqueño.
		</p>
		<p align="justify">
			Durante los procesos de investigación participativa requeridos para estos fines, se conformarán archivos documentales, audios y audiovisuales que conformarán el acervo del Departamento de Patrimonio Inmaterial, que además serán insumos para distintas piezas comunicacionales que difundan las expresiones creativas de las comunidades chaqueñas.
		</p></br></br></br>
		<p align="justify">

			<b>ACCIONES DEL DEPARTAMENTO</b> (de acuerdo al Decreto de su creación, 2402/15)
		</p></br></br>

		<p align="justify">
			* Supervisar la actualización del inventario del Patrimonio Inmaterial de la provincia.
		</p></br>
		<p align="justify">
			* Promover y ejecutar actividades de rescate, conservación y salvaguarda del PCI, articulando con distintas áreas del Instituto, organizaciones públicas y privadas.
		</p></br>
		<p align="justify">
			* Formular y gestionar actividades de capacitación e investigación a los gestores culturales, funcionarios y portadores del patrimonio inmaterial de todo el territorio provincial.
		</p></br>
		<p align="justify">
			* Articular y ejecutar proyectos de difusión sobre información disponible del PCI de la provincia.
		</p>
     </article>
    <!-- ################################################################################################ -->
	</br>
    <footer><a class="btn" href="<?php echo url_for('home/index') ?>">Volver &raquo;</a></footer>
  </div>

</div>

