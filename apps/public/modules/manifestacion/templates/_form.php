<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form action="<?php echo url_for('manifestacion/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
          <?php echo $form->renderHiddenFields(false) ?>
          &nbsp;<a href="<?php echo url_for('manifestacion/index') ?>">Back to list</a>
          <?php if (!$form->getObject()->isNew()): ?>
            &nbsp;<?php echo link_to('Delete', 'manifestacion/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
          <?php endif; ?>
          <input type="submit" value="Save" />
        </td>
      </tr>
    </tfoot>
    <tbody>
      <?php echo $form->renderGlobalErrors() ?>
      <tr>
        <th><?php echo $form['proyecto_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['proyecto_id']->renderError() ?>
          <?php echo $form['proyecto_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['localizacion_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['localizacion_id']->renderError() ?>
          <?php echo $form['localizacion_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['denominacion']->renderLabel() ?></th>
        <td>
          <?php echo $form['denominacion']->renderError() ?>
          <?php echo $form['denominacion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['observaciones']->renderLabel() ?></th>
        <td>
          <?php echo $form['observaciones']->renderError() ?>
          <?php echo $form['observaciones'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['area_expresion_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['area_expresion_id']->renderError() ?>
          <?php echo $form['area_expresion_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['nombre_en_otra_area']->renderLabel() ?></th>
        <td>
          <?php echo $form['nombre_en_otra_area']->renderError() ?>
          <?php echo $form['nombre_en_otra_area'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_resumen']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_resumen']->renderError() ?>
          <?php echo $form['caracteristicas_resumen'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_descripcion']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_descripcion']->renderError() ?>
          <?php echo $form['caracteristicas_descripcion'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_permanencia_transformaciones']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_permanencia_transformaciones']->renderError() ?>
          <?php echo $form['caracteristicas_permanencia_transformaciones'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_transmision']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_transmision']->renderError() ?>
          <?php echo $form['caracteristicas_transmision'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_contexto']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_contexto']->renderError() ?>
          <?php echo $form['caracteristicas_contexto'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_importancia_vida_social']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_importancia_vida_social']->renderError() ?>
          <?php echo $form['caracteristicas_importancia_vida_social'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['caracteristicas_otras_informaciones']->renderLabel() ?></th>
        <td>
          <?php echo $form['caracteristicas_otras_informaciones']->renderError() ?>
          <?php echo $form['caracteristicas_otras_informaciones'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['temporalidad_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['temporalidad_id']->renderError() ?>
          <?php echo $form['temporalidad_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['detalle_temporalidad']->renderLabel() ?></th>
        <td>
          <?php echo $form['detalle_temporalidad']->renderError() ?>
          <?php echo $form['detalle_temporalidad'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['estado_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['estado_id']->renderError() ?>
          <?php echo $form['estado_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['sensibilidad_al_cambio_id']->renderLabel() ?></th>
        <td>
          <?php echo $form['sensibilidad_al_cambio_id']->renderError() ?>
          <?php echo $form['sensibilidad_al_cambio_id'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['amenazas_sobre_la_practica']->renderLabel() ?></th>
        <td>
          <?php echo $form['amenazas_sobre_la_practica']->renderError() ?>
          <?php echo $form['amenazas_sobre_la_practica'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['amenazas_sobre_la_transmision']->renderLabel() ?></th>
        <td>
          <?php echo $form['amenazas_sobre_la_transmision']->renderError() ?>
          <?php echo $form['amenazas_sobre_la_transmision'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['comunidad_se_llama_a_si_misma']->renderLabel() ?></th>
        <td>
          <?php echo $form['comunidad_se_llama_a_si_misma']->renderError() ?>
          <?php echo $form['comunidad_se_llama_a_si_misma'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['comunidad_se_siente_parte_de']->renderLabel() ?></th>
        <td>
          <?php echo $form['comunidad_se_siente_parte_de']->renderError() ?>
          <?php echo $form['comunidad_se_siente_parte_de'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['comunidad_puede_catalogarse_etnicamente']->renderLabel() ?></th>
        <td>
          <?php echo $form['comunidad_puede_catalogarse_etnicamente']->renderError() ?>
          <?php echo $form['comunidad_puede_catalogarse_etnicamente'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['comunidad_puede_catalogarse_por_pertenencia']->renderLabel() ?></th>
        <td>
          <?php echo $form['comunidad_puede_catalogarse_por_pertenencia']->renderError() ?>
          <?php echo $form['comunidad_puede_catalogarse_por_pertenencia'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['descripcion_quienes_tienen_conocimiento']->renderLabel() ?></th>
        <td>
          <?php echo $form['descripcion_quienes_tienen_conocimiento']->renderError() ?>
          <?php echo $form['descripcion_quienes_tienen_conocimiento'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['descripcion_quienes_practican']->renderLabel() ?></th>
        <td>
          <?php echo $form['descripcion_quienes_practican']->renderError() ?>
          <?php echo $form['descripcion_quienes_practican'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['descripcion_entre_quienes_se_transmite']->renderLabel() ?></th>
        <td>
          <?php echo $form['descripcion_entre_quienes_se_transmite']->renderError() ?>
          <?php echo $form['descripcion_entre_quienes_se_transmite'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['evaluacion_problemas_posibilidades']->renderLabel() ?></th>
        <td>
          <?php echo $form['evaluacion_problemas_posibilidades']->renderError() ?>
          <?php echo $form['evaluacion_problemas_posibilidades'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['evaluacion_recomendaciones']->renderLabel() ?></th>
        <td>
          <?php echo $form['evaluacion_recomendaciones']->renderError() ?>
          <?php echo $form['evaluacion_recomendaciones'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['publicar']->renderLabel() ?></th>
        <td>
          <?php echo $form['publicar']->renderError() ?>
          <?php echo $form['publicar'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['created_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_at']->renderError() ?>
          <?php echo $form['created_at'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['updated_at']->renderLabel() ?></th>
        <td>
          <?php echo $form['updated_at']->renderError() ?>
          <?php echo $form['updated_at'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['created_by']->renderLabel() ?></th>
        <td>
          <?php echo $form['created_by']->renderError() ?>
          <?php echo $form['created_by'] ?>
        </td>
      </tr>
      <tr>
        <th><?php echo $form['updated_by']->renderLabel() ?></th>
        <td>
          <?php echo $form['updated_by']->renderError() ?>
          <?php echo $form['updated_by'] ?>
        </td>
      </tr>
    </tbody>
  </table>
</form>
