
<?php use_helper('jQuery'); ?>
<!-- pagination --> 

<nav class="pagination">      

    <ul class='pager'>
        <li class='first-page' >
            <?php echo jq_link_to_remote('<<', 
                    $options = array(
                            'update' => 'CargarResultado',
                            'url'    => 'manifestacion/searchResult?first=false&page=1',                     
                        )
                    );
            
            ?>
        </li>
        <li>            
            <?php echo jq_link_to_remote('<', 
                    $options = array(
                            'update' => 'CargarResultado',
                            'url'    => 'manifestacion/searchResult?first=false&page='.$pager->getPreviousPage()
                        )
                    );
            ?>
        </li>
            <?php foreach ($pager->getLinks() as $page): ?>
                <?php if ($page == $pager->getPage()): ?>
                    <li class='current'> <a> <?php echo $page ?></a></li>
                <?php else: ?>
                    <li>
                        <?php echo jq_link_to_remote( $page, 
                            $options = array(
                                'update' => 'CargarResultado',
                                'url'    => 'manifestacion/searchResult?first=false&page='. $page )
                            );
                        ?>   
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        <li>
            <?php echo jq_link_to_remote( '>', 
                  $options = array(
                       'update' => 'CargarResultado',
                        'url'    => 'manifestacion/searchResult?first=false&page='. $pager->getNextPage() )
                    );
            ?>  
        </li>
        <li class='last-page'>
            <?php echo jq_link_to_remote( '>>', 
                    $options = array(
                        'update' => 'CargarResultado',
                        'url'    => 'manifestacion/searchResult?first=false&page='. $pager->getLastPage() )
                    );
            ?>  
        </li>
    </ul>

</nav>

