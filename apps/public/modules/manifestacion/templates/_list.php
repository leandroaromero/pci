
    
    <div class="scrollable">
        <table>  
          <thead>
            <tr>
              <th>Proyecto</th>
              <th>Denominación</th>
              <th>Localización</th>
              <th></th>
            </tr>

           </thead>
          <tbody>
             <?php foreach ($result as $manifestacion): ?>
              <tr>
                  <td><?php echo $manifestacion->getProyecto() ?></td>
                  <td><?php echo $manifestacion->getDenominacion() ?></td>
                  <td><?php echo $manifestacion->getLocalizacion() ?></td>
                  <td>
                    <?php
                                echo link_to(image_tag('open.gif'),'manifestacion/show?id='.$manifestacion->getId(),
                                array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=4000,height=600,left=320,top=0'), 'size=100x100'))
                            ?>

                  </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
   </div>      
     

