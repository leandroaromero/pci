<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('jQuery'); ?>

<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <h1>Manifestaciones</h1>
      <p>Le presentamos el listado de manifestaciones de Provincia del Chaco</p>
      
      <div id="comments">       
        <form action="#" method="post">
          <div class="one_third first">
            <label for="Denominacion"><?php echo $form['general']->renderLabel() ?></label>
            <?php echo $form['general']->renderError() ?>
            <?php echo $form['general']->render() ?>
          </div>
          <div class="one_third">
            <label for="Proyecto">Proyecto</label>
            <?php echo $form['proyecto_id'] ?>
          </div>
          <div class="one_third">
            <label for="Temporalidad"> Temporalidad </label>
            <?php echo $form['temporalidad_id'] ?>
          </div>
          <div id="comments">
              <label for="Ambitos">Ambitos </label>
            <?php echo $form['ambito'] ?>
          </div>
          
          <div>
                <?php echo jq_submit_to_remote( ' submit'   , __('Buscar'),
                    $options = array(
                        'update' => 'CargarResultado',
                        'url'    => 'manifestacion/searchResult?first=true',
                    ), 
                      $options_html = array('name'=>'submit', 'style'=>'width:190px')
                    )?>
          </div>
        </form>
      </div>
      <!-- ################################################################################################ -->

        <div id="CargarResultado"> 

          <?php include_partial('manifestacion/list', array('result' => $pager->getResults())) ?>
          
          <div class="clear"></div>
          
          <div style="display: inline;float: right;">
            <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
            <?php if ($pager->haveToPaginate()): ?>
                <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->  getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
            <?php endif; ?>
          </div>  
          <?php if ($pager->haveToPaginate()): ?>
            <?php include_partial('manifestacion/pagination', array('pager' => $pager)) ?>
          <?php endif; ?>
        </div>               
      </div>


 <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>














































