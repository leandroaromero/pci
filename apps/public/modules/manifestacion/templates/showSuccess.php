<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <h1><?php echo $manifestacion->getDenominacion() ?></h1>

      <p align="justify" >
        <?php echo $manifestacion->getCaracteristicasResumen() ?>
      </p>    
      
    <!-- ################################################################################################ -->
    </div>
    <!-- / main body -->
    <div class="clear"></div>
    <footer>
      <a class="btn" onclick="window.close();" >Cerrar &raquo;

      </a>

    </footer>

  </main>
</div>



<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
    <!--script src="js/jquery.lint.js" type="text/javascript" charset="utf-8"></script-->
    <link rel="stylesheet" href="/pci/web/js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />    
    <script src="/pci/web/js/prettyPhoto/js/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="/pci/web/js/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    



<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <div id="gallery">
        <figure>
          <header class="heading">Galería de Imágenes</header>
            <ul  class="gallery clearfix">
              <?php $i=3; foreach ($manifestacion->getImagenes() as $pic):?>
                 <?php ++$i; $odd= fmod(  $i, 4);
                    if(  $odd == 0 ):?>
                      <li class="one_quarter first" >  
                    <?php else: ?>   
                      <li class="one_quarter">
                    <?php endif?>                        
                      <a   style="background: #444 "href="/pci/web/uploads/digital/<?php echo $pic->getArchivo()?>" rel="prettyPhoto[gallery2]" class="fancybox" title="<?php echo  'titulo'?>">
                        <center>
                          <?php echo image_tag('../uploads/digital/'.$pic->getArchivo(), 'style=" height: 148px; max-width: 204px"')?>
                        </center> 
                      </a>
                    </li>
              <?php endforeach;?>    
            </ul>
        </figure>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    
  </main>
</div>


<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <div id="gallery">
        <figure>
          <header class="heading">Galería de Audios</header>
            <ul  class="gallery clearfix">
              <?php $i=2; foreach ($manifestacion->getAudios() as $pic):?>
                 <?php ++$i; $odd= fmod(  $i, 3);
                    if(  $odd == 0 ):?>
                      <li class="one_third first" >  
                    <?php else: ?>   
                      <li class="one_third">
                    <?php endif?>   
                      <?php $dir =  (string)  '/pci/web/uploads/digital/'.$pic->getArchivo(); ?>
                        <audio controls>
                          <source src="<?php echo $dir?>" type="audio/mpeg">
                           Your browser does not support the audio element.
                        </audio>
                    </li>
              <?php endforeach;?>    
            </ul>
        </figure>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>



<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <div id="gallery">
        <figure>
          <header class="heading">Galería de Videos</header>
            <ul  class="gallery clearfix">
              <?php $i=2; foreach ($manifestacion->getVideos() as $pic):?>
                 <?php ++$i; $odd= fmod(  $i, 3);
                    if(  $odd == 0 ):?>
                      <li class="one_third first" >  
                    <?php else: ?>   
                      <li class="one_third">
                    <?php endif?>   
                      <?php $dir =  (string)  '/pci/web/uploads/digital/'.$pic->getArchivo(); ?>
                        <video class="bordered-feature-image" width="300" height="160" controls >
                          <source src="<?php echo $dir?>" type="video/mp4">
                          Your browser does not support the video tag.
                        </video>
                    </li>
              <?php endforeach;?>    
            </ul>
        </figure>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </main>
</div>

<div class="wrapper row2">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <div id="gallery">
        <figure>
          <header class="heading">Galería de Documentos</header>
            <ul  class="gallery clearfix">
              <?php $i=1; foreach ($manifestacion->getPdfs() as $pic):?>
                 <?php ++$i; $odd= fmod(  $i, 2);
                    if(  $odd == 0 ):?>
                      <li class="one_half first" >  
                    <?php else: ?>   
                      <li class="one_halt">
                    <?php endif?>   
                      <?php $dir =  (string)  '/pci/web/uploads/digital/'.$pic->getArchivo(); ?>
                      <embed src="<?php echo $dir?>" type="application/pdf" width="400" height="500"></embed>
                    </li>
              <?php endforeach;?>    
            </ul>
        </figure>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
  </main>
</div>

 <!--<div class="wrapper row2">
  <section class="hoc container clear"> 

     <?php foreach ($manifestacion->getObjetosTipos() as $key1 => $values): ?>

    
    
     <?php $i=2; foreach ($values as $key => $value): ?>
     
      <?php ++$i; $odd= fmod(  $i, 3);
         if(  $odd == 0 ):?>
          <div class="group latest">
        <article class="one_third_first">
      <?php else: ?>   
        <article class="one_third">
      <?php endif?>    

        <!-- ################################################################################################ -->
   <!--       <img src="images/demo/390x260.png" alt="">
        <div class="txtwrap">
          <h6 class="heading">Eu tellus accumsan</h6>
          <time class="font-xs block btmspace-10" datetime="2045-04-10">Tuesday, 10<sup>th</sup> April 2045</time>
          <p>Cursus risus est in ullamcorper mi pharetra sed vivamus interdum pulvinar sem efficitur sagittis in [&hellip;]</p>
          <footer><a href="#">Read More</a></footer>
          </div>
        <!-- ################################################################################################ -->
  <!--    </article>

      <?php if( $odd == 2 ):?>
          </div >
      <?php endif?>

    <?php endforeach; ?>
<!--    <div class="clear"></div>
</section>
</div>  
  
<?php endforeach; ?>
-->


  <script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
      $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',theme:'light_square',slideshow:4000, autoplay_slideshow: true});
      $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'fast',slideshow:10000});
      
      $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
        custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
        changepicturecallback: function(){ initialize(); }
      });

      $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
        custom_markup: '<div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
        changepicturecallback: function(){ _bsap.exec(); }
      });
    });
    </script>
    