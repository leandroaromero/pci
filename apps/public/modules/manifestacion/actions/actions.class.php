<?php

/**
 * manifestacion actions.
 *
 * @package    pci
 * @subpackage manifestacion
 * @author     Leandro Antonio Romero
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class manifestacionActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {

    $this->getUser()->setAttribute('menu','li_mani');
    $this->form = new ManifestacionAdminFormFilter();
    $filtro = array();


    if($request->getParameter('nombre') ){

      $a = Doctrine_Core::getTable('Ambitos')
      ->createQuery('aa')
      ->where('aa.nombre like ?' , '%'.$request->getParameter('nombre').'%' );
         
      if( $a->execute()->count() > 0 )
        { 
          $ambito=  $a->fetchOne();
          $this->form->setDefault('ambito', $ambito->getId());
          $filtro['ambito']=$ambito->getId();
      }
    }

    $this->getUser()->getAttribute('search_manifestacion', $this->form);   

    $this->manifestaciones = $this->getResult($filtro);
    
    $this->pager = new sfDoctrinePager( 'Manifestacion', 4);
    $this->pager->setQuery($this->manifestaciones);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();



  }

  public function executeShow(sfWebRequest $request)
  {
    $this->manifestacion = Doctrine_Core::getTable('Manifestacion')->find(array($request->getParameter('id')));
    $this->manifestacion->setObjetos();
    $this->forward404Unless($this->manifestacion);
    $this->setLayout('layout-manifestacion');
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->forward404Unless(false);
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless(false);
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless(false);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless(false);
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($manifestacion = Doctrine_Core::getTable('Manifestacion')->find(array($request->getParameter('id'))), sprintf('Object manifestacion does not exist (%s).', $request->getParameter('id')));
    $manifestacion->delete();

    $this->redirect('manifestacion/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $this->forward404Unless(false);
  }
/*
  public function executeSearchResult(sfWebRequest $request)
  {
      
    $this->result = Doctrine_Core::getTable('Manifestacion')
      ->createQuery('a')
      ->orderBy('a.denominacion');
    
    $this->pager = new sfDoctrinePager( 'Manifestacion', 4);
    $this->pager->setQuery($this->result);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();

  }  
*/


  public function executeSearchResult(sfWebRequest $request)
  {

    $filtros= $request->getParameter('manifestacion_filters');
    
    if( $request->getParameter('first') == 'false'){
      $filtros = $this->getUser()->getAttribute('search_manifestacion');  
    }else{
      $this->getUser()->setAttribute('search_manifestacion', $filtros);
    }

    $this->result = $this->getResult($filtros);

    $this->pager = new sfDoctrinePager( 'Manifestacion', 4);
    $this->pager->setQuery($this->result);
    $this->pager->setPage($request->getParameter('page', 1));
    $this->pager->init();
      
  }
  protected function getResult( $search ){

    $manifestacions = Doctrine_Core::getTable('Manifestacion')
      ->createQuery('a');

      $manifestacions->andWhere('a.publicar = ? ',  1 );    
      if( isset($search['general']) && ($search['general'] != '' ) && ($search['general'] != null )){      
        $manifestacions->andWhere('a.denominacion like ?', '%'.$search['general'].'%') ;
      }
      if( isset($search['proyecto_id']) && ($search['proyecto_id'] != '' )&& ($search['proyecto_id'] != null ) ){
        $manifestacions->andWhere('a.proyecto_id = ?', $search['proyecto_id']); 
      }
      if( isset($search['temporalidad_id']) && ($search['temporalidad_id']!= '')&& ($search['temporalidad_id']!= null) ){
        $manifestacions->andWhere('a.temporalidad_id = ?', $search['temporalidad_id']); 
      }  

      if( isset($search['ambito']) && ($search['ambito']!= '') && ($search['ambito']!= '') ){
        $manifestacions->leftJoin('a.ManifestacionAmbitos ma' )
                       ->leftJoin( 'ma.Ambitos aa' )
            ->leftJoin( 'aa.Ambitos bb' )
            ->leftJoin( 'bb.Ambitos cc' )
            ->leftJoin( 'cc.Ambitos dd' )
            ->leftJoin( 'dd.Ambitos ee' )
            ->leftJoin( 'ee.Ambitos ff' )
            ->andWhere( 'aa.id =? OR bb.id =? OR cc.id =? OR dd.id =? OR ee.id =? OR ff.id =?', 
              array($search['ambito'], $search['ambito'], $search['ambito'], $search['ambito'], $search['ambito'], $search['ambito']) ) ;      
          }




   //   $manifestacions->orderBy('a.denominacion asc');    
       //   $manifestacions->where('a.publicar != ? ',  0 );
    return $manifestacions;

  }


}
