<?php

/**
 * Manifestacion filter form.
 *
 * @package    pci
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ManifestacionAdminFormFilter extends BaseManifestacionFormFilter
{
  public function configure()
  {
  	
  	unset(
      	$this['localizacion_id'],        
      	$this['observaciones'],
  		$this['area_expresion_id'],
  		$this['nombre_en_otra_area'],
      	$this['caracteristicas_resumen'],        
      	$this['caracteristicas_descripcion'],
  		$this['caracteristicas_permanencia_transformaciones'],
  		$this['caracteristicas_transmision'],
      	$this['caracteristicas_contexto'],        
      	$this['caracteristicas_importancia_vida_social'],
  		$this['caracteristicas_otras_informaciones'],
  		$this['detalle_temporalidad'],
      	$this['estado_id'],        
      	$this['sensibilidad_al_cambio_id'],
  		$this['amenazas_sobre_la_practica'],
  		$this['amenazas_sobre_la_transmision'],
      	$this['comunidad_se_llama_a_si_misma'],        
      	$this['comunidad_se_siente_parte_de'],
  		$this['comunidad_puede_catalogarse_etnicamente'],
  		$this['comunidad_puede_catalogarse_por_pertenencia'],
      	$this['descripcion_quienes_tienen_conocimiento'],        
      	$this['descripcion_quienes_practican'],
  		$this['descripcion_entre_quienes_se_transmite'],
  		$this['evaluacion_problemas_posibilidades'],
      	$this['evaluacion_recomendaciones'],              	
      	$this['updated_at'],        
      	$this['updated_by'],
  		$this['created_by'],
  		$this['created_at']
  	);
  $this->widgetSchema['general']= new sfWidgetFormInput(
                                        array('label'=>'Denominación',
                                          ));

  $this->widgetSchema['proyecto_id']->setAttributes( array( 
               'style'=>'width:300px;height:40px',
              )); 
  
  $this->widgetSchema['temporalidad_id']->setAttributes( array( 
               'style'=>'width:300px;height:40px',
              )); 
 
  $this->widgetSchema['temporalidad_id']->setOption('table_method', 'getTemporalidadOrden');
  $this->widgetSchema['proyecto_id']->setOption('table_method', 'getProyectoOrden');

	$this->widgetSchema['ambito'] = new sfWidgetFormDoctrineChoice(array(
        'model'   => 'Ambitos',
        'table_method' => 'getAmbitosOrdenados',
        'add_empty' => true
      ),
         array( 
               'style'=>'width:99%;height:40px',
              )
);

    $this->validatorSchema['ambito']  = new sfValidatorPass(array('required' => false));

  }
  public function addAmbitoColumnQuery(Doctrine_Query $query, $field, $value)
  {
                $query->leftJoin($query->getRootAlias().'.ManifestacionAmbitos mas' )
                      ->andWhere('mas.ambitos_id = ?', $value);
   
  }


}
