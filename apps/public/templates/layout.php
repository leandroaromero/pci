<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/pci/web/images/logo2.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

  
    <script type="text/javascript" >
    function setMenu(valor)
    {
        var   valores = $.ajax({
                   type: 'GET',
                   url: '<?php echo url_for('home/setMenu')?>'+'?menu='+valor,
                   async: false
                }).responseText;
      }
  </script>                                       
  <script language="javascript" type="text/javascript" >
    window.onload = function()
    {
        document.getElementById('li_mani').className='';

        <?php if ( $sf_user->getAttribute('menu') ) :?>
              document.getElementById('li_home').className='';
              document.getElementById('<?php echo $sf_user->getAttribute('menu')?>').className= 'active';
        <?php endif;?>       
        };
  </script>   

  </head>
  <body id="top" >

    <div class="wrapper row1">
        <header id="header" class="hoc clear"> 
          <!-- ################################################################################################ -->
          <div id="logo" class="fl_left">
            <h1 onclick="setMenu('li_home')" ><a href="<?php echo url_for('@homepage')?>">
              
            <i><b>P</b>atrimonio <b>C</b>ultural <b>I</b>nmaterial</i>
          </a></h1>
          </div>
          <nav id="mainav" class="fl_right">
            <ul class="clear">
              <li class="active" id="li_home" onclick="setMenu('li_home')" >
                  <a href="<?php echo url_for('@homepage')?>">Inicio</a>
              </li>
              <li id="li_mani" onclick="setMenu('li_mani')">
                <a  href="<?php echo url_for('manifestacion/index')?>">Manifestaciones</a>
              </li>

            <!--  <li><a class="drop" href="#">Pages</a>
                <ul>
                  <li><a href="pages/gallery.html">Gallery</a></li>
                  <li><a href="pages/full-width.html">Full Width</a></li>
                  <li><a href="pages/sidebar-left.html">Sidebar Left</a></li>
                  <li><a href="pages/sidebar-right.html">Sidebar Right</a></li>
                  <li><a href="pages/basic-grid.html">Basic Grid</a></li>
                </ul>
              </li>
              <li><a class="drop" href="#">Dropdown</a>
                <ul>
                  <li><a href="#">Level 2</a></li>
                  <li><a class="drop" href="#">Level 2 + Drop</a>
                      <ul>
                        <li><a href="#">Level 3</a></li>
                        <li><a href="#">Level 3</a></li>
                        <li><a href="#">Level 3</a></li>
                      </ul>
                  </li>
                  <li><a href="#">Level 2</a></li>
                </ul>
              </li>
              <li><a href="#">Link Text</a></li>
              <li><a href="#">Link Text</a></li>-->
            </ul>
          </nav>
              <!-- ################################################################################################ -->
        </header>
      </div>
      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
     

          <?php echo $sf_content ?>

      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <!-- ################################################################################################ -->
      <div class="wrapper row4">
        <footer id="footer" class="hoc clear"> 
        <!-- ################################################################################################ -->
          <div class="one_third first">
            <h6 class="heading">Patrimonio Cultural Inmaterial</h6>
              <ul class="nospace btmspace-30 linklist contact">
                <li><i class="fa fa-map-marker"></i>
                    <address>
                      Avenida Wilde 40, 
                      3500 ,Ciudad de Resistencia
                    </address>
                </li>
                <li><i class="fa fa-phone"></i> +36 24 453 094</li>
                <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
              </ul>



      <ul class="faico clear">
        <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
        <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
        <li><a class="faicon-vk" href="#"><i class="fa fa-vk"></i></a></li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Eget efficitur sodales</h6>
      <ul class="nospace linklist">
        <li>
          <article>
            <h2 class="nospace font-x1"><a href="#">Ac mauris egestas ipsum</a></h2>
            <time class="font-xs block btmspace-10" datetime="2045-04-06">Friday, 6<sup>th</sup> April 2045</time>
            <p class="nospace">At commodo dolor nulla eu eleifend velit eu posuere neque phasellus ut [&hellip;]</p>
          </article>
        </li>
        <li>
          <article>
            <h2 class="nospace font-x1"><a href="#">Pretium tempus interdum</a></h2>
            <time class="font-xs block btmspace-10" datetime="2045-04-05">Thursday, 5<sup>th</sup> April 2045</time>
            <p class="nospace">Risus luctus dapibus libero in bibendum nulla ut sodales fermentum eros [&hellip;]</p>
          </article>
        </li>
      </ul>
    </div>
    <div class="one_third">
      <h6 class="heading">Pretium eleifend mauris</h6>
      <p class="nospace btmspace-30">Lobortis aliquam eu placerat dui sed fermentum nibh eu nunc semper non.</p>
      <form method="post" action="#">
        <fieldset>
          <legend>Newsletter:</legend>
          <input class="btmspace-15" type="text" value="" placeholder="Name">
          <input class="btmspace-15" type="text" value="" placeholder="Email">
          <button type="submit" value="submit">Submit</button>
        </fieldset>
      </form>
    </div>
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row5">
  <div id="copyright" class="hoc clear"> 
    <!-- ################################################################################################ -->
    <p class="fl_left">Copyright &copy; 2019 - All Rights Reserved - <a href="#">Patrimonio Cultural Inmaterial</a></p>
    <p class="fl_right">Template by <a target="_blank" href="https://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->

  </body>
</html>
